# import sys
# from os import path

from setuptools import find_packages, setup

import versioneer

setup(
    name="bessyii_devices_instantiation",
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description="Beamline device instantiation",
    url="https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices_instantiation",
    author="Daniel Tomecki",
    author_email="daniel.tomecki@helmholtz-berlin.de",
    # license='MIT',
    packages=find_packages(exclude=["docs"]),
    install_requires=[
        "bluesky>=1.13",
        "ophyd>=1.9.0",
        "ophyd-async>=0.9.0",
        "databroker>=1.2.5",
        "happi>=2.5.0",
        "colorama>=0.4.6",
        "raypyng>=1.2.4",
        "tqdm>=4.66.2",
        "pyyaml>=6.0.1",
        "pytest>=8.1.1",
    ],
    # zip_safe=False
)
