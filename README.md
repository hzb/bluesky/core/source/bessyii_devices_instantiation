# bessyii devices instantiation

## Table of Contents
- [bessyii devices instantiation](#bessyii-devices-instantiation)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
  - [Installation and usage](#installation-and-usage)
  - [Access to the metadata](#access-to-the-metadata)
  - [Create the device configuration y(a)ml file](#create-the-device-configuration-yaml-file)
  - [Cheat sheet for maintenance purposes](#cheat-sheet-for-maintenance-purposes)
    - [Bluesky deployment (ansible)](#bluesky-deployment-ansible)
    - [Bluesky startup scripts](#bluesky-startup-scripts)
    - [Bluesky container](#bluesky-container)


<a name="Description"></a>
## Description

This project enables instantation of remote-controlled devices that are used on bessyii beamlines. The instantiation process is based on the functionalities of the [happi](https://pcdshub.github.io/happi) library. In this context, the concept of happi containers is used to define how the object it represents is to be instantiated through class names, arguments and keyword arguments.
On the other hand, it allows any number of properties to be added, which are represented as metadata information of an instantiated device. The metadata can describe various parameters of a device involved in an experiment.
The properties that represent metadata information are managed in the device configuration file y(a)ml or/and the rml-file.

<a name="Installation and usage"></a>
## Installation and usage

1. Installation and usage in the context of bluesky container

    If the package "bessyii_devices_instantiation" is to be used in the context of bluesky container, make sure that bluesky has been deployed to a specific beamline. To do this, follow the instructions from [ansible](https://codebase.helmholtz.cloud/hzb/ansible) to deploy bluesky to a specific beamline.

    The following configuration files must be prepared before starting the bluesky container at the beamline.

    - Create the device configuration y(a)ml file. Read [Create the device configuration y(a)ml file](#create-the-device-configuration-yaml-file). An example of [device configuration file](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/demo_beamline/source/beamlinetools/-/blob/2827_happi/beamlinetools/BEAMLINE_CONFIG/beamline_devices/beamline_devices.yml?ref_type=heads).
    
        Target directory: "~/bluesky/beamlinetools/beamlinetools/BEAMLINE_CONFIG/beamline_devices".

    - Create the rml file (example of [rml file](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/demo_beamline/source/beamlinetools/-/blob/2827_happi/beamlinetools/BEAMLINE_CONFIG/beamline_devices/optics_trial.rml?ref_type=heads)) if one of the devices is to retrieve metadata from the rml file. This is the case if any device configured in the device configuration file sets "apply_rml_on" to "True". If no device sets "apply_rml_on" to "True", it is not necessary to create a rml file.
    
        Target directory: "~/bluesky/beamlinetools/beamlinetools/BEAMLINE_CONFIG/beamline_devices".
    
        Content of the rml file must be adapted to the devices (objects) that are used on the respective beamline.

        In the rml file, the value of the "name" attribute of the object must correspond to the device "instance_name" configured in the device configuration file so that the metadata to be extracted can be found in the rml file for this specific device.
Example:
        - device instance name: "m1"
        - device is a Plane Mirror

        `
        <object name="m1" type="Plane Mirror">
        `

        The extraction of metadata from the rml file is currently supported for the following device types: Toroid, Plane Mirror, Cylinder, Ellipsoid.


    - Create device instantiation setup file (example of [device instantiation setup file](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/demo_beamline/source/beamlinetools/-/blob/2827_happi/beamlinetools/BEAMLINE_CONFIG/beamline_devices/dev_instantiation_setup.yml?ref_type=heads)) using the following file name: "dev_instantiation_setup.yml".
    
        Target directory: "~/bluesky/beamlinetools/beamlinetools/BEAMLINE_CONFIG/beamline_devices".
        
        
        
        Adapt the content of the the device instantiation setup file to the specific file names of the device configuration file and the rml file.

        -   The value of key: "RML_FILE_NAME" can be an arbitrary string (including an empty string) if rml file is not in use. This is the case if all devices configured in device configuration file set "apply_rml_on" to "False".

        -   The value of key: "HAPPI_DB_FILE_NAME" can be set to "db.json" and does not have to be changed. The Happi db file is automatically created with a name specified by the value of "HAPPI_DB_FILE_NAME".

    The bluesky container can be started by using:

    ```
    bluesky_start
    ```


    After starting the "bluesky_container", all instantiated devices (configured as active devices in the device configuration file) are visible in the IPython session that has been started. Instantiated devices can be accessed via the "devices_dictionary" dictionary or directly via the name of the device instance. For example, for a device with the instance name v3, both of the following expressions return the same object (instantiated device):

    ```
    devices_dictionary["v3"]
    ```

    ```
    v3
    ```

1. Installation and usage outside the bluesky container

    - Clone the main branch of the project [bessyii_devices_instantiation](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices_instantiation)

    - Create and activate an environemet with python_version >= "3.10"
    - Install bessyii_devices_instantiation package in this environement. For this, depending on your environement (development/production), you can use either the editable or non-editable mode:

        If you are working in a development and not a production environment use

        ```
        cd "path to cloned bessyii_devices_instantiation package"
        pip install .
        ```
        or
        ```
        cd "path to cloned bessyii_devices_instantiation package"
        pip install -e .
        ```
    - Select arbitrary target directory path on your system for storing following files (e.g. /home/user/test/):
        - device configuration file
        - rml file
        - happi db json file
        - device instantiation setup file
    - Assign selected target directory path to a variable "TARGET_DIR", 
        ```
        TARGET_DIR=/home/user/test/
        ```
    - Set environement variables by executing the following:
        ```
        export _DEV_CFG_FILE_DIR_PATH=$TARGET_DIR
        export _RML_FILE_DIR_PATH=$TARGET_DIR
        export _HAPPI_DB_FILE_DIR_PATH=$TARGET_DIR
        export _DEV_INSTANTIATION_SETUP_FILE_DIR_PATH=$TARGET_DIR
        export DEV_INSTANTIATION_SETUP_FILE_NAME=dev_instantiation_setup.yml
        ```
    - Create device instantiation setup file in the target directory (example of [device instantiation setup file](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/demo_beamline/source/beamlinetools/-/blob/2827_happi/beamlinetools/BEAMLINE_CONFIG/beamline_devices/dev_instantiation_setup.yml?ref_type=heads)) using the following file name: "dev_instantiation_setup.yml".
    - Create the device configuration y(a)ml file in the target directory. Read [Create the device configuration y(a)ml file](#create-the-device-configuration-yaml-file). An example of [device configuration file](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/demo_beamline/source/beamlinetools/-/blob/2827_happi/beamlinetools/BEAMLINE_CONFIG/beamline_devices/beamline_devices.yml?ref_type=heads).
    - Create the rml file in the target directory (example of [rml file](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/demo_beamline/source/beamlinetools/-/blob/2827_happi/beamlinetools/BEAMLINE_CONFIG/beamline_devices/optics_trial.rml?ref_type=heads)) if one of the devices is to retrieve metadata from the rml file. This is the case if any device configured in the device configuration file sets "apply_rml_on" to "True". If no device sets "apply_rml_on" to "True", it is not necessary to create a rml file.

    - Adapt the content of the the device instantiation setup file to the specific file names of the device configuration file and the rml file.

        - The value of key: "RML_FILE_NAME" can be an arbitrary string (including an empty string) if rml file is not in use. This is the case if all devices configured in device configuration file set "apply_rml_on" to "False".

        - The value of key: "HAPPI_DB_FILE_NAME" can be set to "db.json" and does not have to be changed. The Happi db file is automatically created with a name specified by the value of "HAPPI_DB_FILE_NAME".

    - In your application that is to use the functionalities of the "bessyii_devices_instantiation" package, import the functions that are responsible for device instantiation from the "beamline_devices" module:

        ```
        from bessyii_devices_instantiation.beamline_devices import instantiate, wait_for_device_connection
        ```

    - To instantiate devices call the function:

        ```
        devices_dictionary: dict[object] = instantiate()
        ```
        The instantiation is based on the settings that were previously made in the device instantiation setup file.

        If you want to refer explicitly to specific configuration files, please call:
        ```
        devices_dictionary: dict[object] = instantiate(dev_cfg_file_path: str, db_file_path: str, rml_file_path: str)
        ```

        `dev_cfg_file_path` is a path to device configuration y(a)ml file (example of [device configuration file](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/demo_beamline/source/beamlinetools/-/blob/2827_happi/beamlinetools/BEAMLINE_CONFIG/beamline_devices/beamline_devices.yml?ref_type=heads)).

        `db_file_path` is a path to data base json file. The name of the file can be set to "db.json". This file does not need to exist as it is created automatically.

        `rml_file_path` is a path to an rml file (example of [rml file](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/demo_beamline/source/beamlinetools/-/blob/2827_happi/beamlinetools/BEAMLINE_CONFIG/beamline_devices/optics_trial.rml?ref_type=heads)). This file must be present if any of the devices configured in the device configuration file sets "apply_rml_on" to "True". If no device configured in the device configuration sets "apply_rml_on" to "True", the `rml_file_path` parameter can by an arbitrary string including an empty string.

        In the rml file, the value of the "name" attribute of the object must correspond to the device "instance_name" configured in the device configuration file so that the metadata to be extracted can be found in the rml file for this specific device. Example:
        - device instance name: "m1"
        - device is a Plane Mirror

        `
        <object name="m1" type="Plane Mirror">
        `

        The extraction of metadata from the rml file is currently supported for the following device types: Toroid, Plane Mirror, Cylinder, Ellipsoid.


    - To wait for all instantiated devices to connect, call up the function:
        ```
        wait_for_device_connection(devices_dictionary: dict[str, object], run_engine: Optional[RunEngine] = None)
        ```
        - The `devices_dictionary` parameter, which is passed to the `wait_for_device_connection` function, is a dictionary that is returned by the `instantiate()` function.
        - The parameter `run_engine`, i.e. the instance of the bluesky RunEngine, which is passed to the `wait_for_device_connection` function, is required if `ophyd_async` (asynchronous) devices are present in the `devices_dictionary`. If only `ophyd` (synchronous) devices are present in the `device_dictionary`, this parameter can be omitted.

    Instantiated devices can be accessed via a dictionary that is returned by the instantiate() function. For a dictionary e.g. "devices_dictionary" calling of 
`
devices_dictionary["v3"] 
`
returns an object (instantiated device) of the device with instance name "v3".

    To achieve "direct" access to instantiated devices, global variables can be created in your application. This means that when v3 is called, the object (instantiated device) for the device with the instance name "v3" is addressed "directly".

    To gain direct access to the instantiated devices, the following function "add_to_global_scope(devices_dictionary, globals())" can be called in your application:

    ```
    def add_to_global_scope(objects: dict, global_scope: dict):
        for obj_name, obj_instance in objects.items():
            # Before adding a new object to the scope of global variables assert that it is does not already exist.
            # We do not want to overwrite already existing objects in the global scope

            if obj_name not in global_scope:
                global_scope[obj_name] = obj_instance
            else:
                raise ValueError(
                    f"Add to the global scope an object with the name: {obj_name} FAILED. Object with the name: {obj_name} already exists in the scope of global variables."
            )
    ```

<a name="Access to the metadata"></a>
## Access to the metadata

Regardless of where metadata was obtained from, whether from rml file or device configuration file, to acces the metadata e.g. `materialCoating1` or `worldPosition` associated with a specific device instance e.g. `m1` call:

```
m1.md.materialCoating1
```
```
m1.md.worldPosition["x"]
m1.md.worldPosition["y"]
m1.md.worldPosition["z"]
```

All metadata associated with a specific device instance e.g. `m1` can be retrieved by using the function get_attributes():

```
m1.md.get_attributes()
```

Since the get_attributes() function returns a dictionary containing all metadata associated with a particular device instance, we can also access all metadata information such as "materialCoating1" or "worldPosition" in this way:

```
m1.md.get_attributes()["materialCoating1"]
```
```
m1.md.get_attributes()["worldPosition"]["x"]
m1.md.get_attributes()["worldPosition"]["y"]
m1.md.get_attributes()["worldPosition"]["z"]
```

<a name="Create the device configuration y(a)ml file"></a>
## Create the device configuration y(a)ml file

This chapter serves as a guide for configuring devices in the beamline device configuration y(a)ml file. Proper configuration ensures successful device instantiation and operation within the beamline environment.

The device configuration file y(a)ml consists of YAML blocks that represent devices and each contain mandatory and optional properties. Ensure all mandatory properties are provided for each device and customize optional properties as needed for specific requirements.

In general, you may be confronted with one of three tasks:

- Add a device:
  - Append a new YAML block with the device properties.
  - Ensure all nine mandatory properties are included.
  - Add any optional properties as needed.
  - Validate the YAML structure to ensure correctness.

- Remove a device:
  - Locate the YAML block for the device to be removed.
  - Delete the entire block.

- Configure a device:
  - Locate the YAML block for the device to be configured.
  - Modify the key/value pairs as needed.
  - Ensure all mandatory properties remain valid.

Below you will find a description of the mandatory and optional properties:

- Nine mandatory properties:
    1) type
        - Description: Device type
        - Requirements: Mandatory, not empty, not unique
        - Example: PositionerBessyValve
    2) active
        - Description: Indicates if the device instantiation is active/enabled
        - Requirements: Mandatory, not empty, not unique
        - Example: True or False
    3) class
        - Description: Class name necessary to instantiate the object
        - Requirements: Mandatory, not empty, not unique
        - Example: bessyii_devices.valve.PositionerBessyValve
    4) prefix
        - Description: Name of the Process Variable (PV)
        - Requirements: Mandatory, not empty, unique with exceptions, i.e. some of the devices use the same prefix
        - Example: V03V11B001L:
    5) instance_name
        - Description: Device instance name
        - Requirements: Mandatory, not empty, unique
        - Example: v3
    6) class_kwargs
        - Description: Keyword arguments required for class instantiation
        - Requirements: Mandatory, can be empty if not required, i.e. empty list: []
        - Example:
        
            ```
            class_kwargs:
                - read_attrs:
                    - "readback"
                    - "reset"
                - labels:
                    - "detectors"
                - label: detector
                - uuid: "{{uuid_fp}}"
                - is_pydantic: True
                - PI: 3.1415
                - sel_option: 4
            ```
            - class_kwargs = {'PI': 3.1415, 'is_pydantic': True, 'label': 'detector', 'labels': ['detectors'], 'read_attrs': ['readback', 'reset'], 'sel_option': 4, 'uuid': '{{uuid_fp}}'}
            ```
            class_kwargs: []
            ```
            - class_kwargs = {}
            ```
            class_kwargs:
            ```
            - class_kwargs = {}
    7) class_args
        - Description: Positional arguments required for class instantiation
        - Requirements: Mandatory, can be empty if not required, i.e. empty list: []
        - Example:

            ```
            class_args:
                - "circle"
                - {"r": 23.67}
                - 3.1415
                - 2
                - True
            ```
            - class_args=['circle', {'r': 23.67}, 3.1415, 2, True]
            ```
            class_args: []
            ```
            - class_args=[]
            ```
            class_args:
            ```
            - class_args=[]
    8) connection_timeout
        - Description: Timeout value [s] used in the wait_for_connection function
        - Requirements: Mandatory, not empty, not unique
        - Example: 5.0 (float value)
    9) apply_rml_on
        - Description: Determines if metadata should be extracted from the RML file
        - Requirements: Mandatory, not empty, not unique
        - Example:  True or False

- Any number of optional properties:
    1) baseline
        - Description: Indicates if the device should be included in the baseline readings
        - Requirements: Optional, not empty, not unique
        - Example: True or False
    2) silent
        - Description: Indicates if the device should be silent during runs
        - Requirements: Optional, not empty, not unique
        - Example: True or False
    3) metadata
        - Description: Arbitrary number of metadata key-value pairs
        - Requirements: Optional, not empty, not unique
        - Example of three metadata key-value pairs:

            ```
            my_meta_data: True
            my_meta_data2: -123456.4567
            my_meta_data3: Hello bluesky
            ```
<a name="Cheat sheet for maintenance purposes"></a>
## Cheat sheet for maintenance purposes

### Bluesky deployment (ansible)

The file deploy_bluesky_vars.yml, which is part of the Bluesky deployment project ([ansible](https://codebase.helmholtz.cloud/hzb/ansible)), contains, among others, following definitions relating to the device instantiation process:

```
DEV_CFG_FILE_DIR_PATH: "{{ beamlinetools_repo_dest }}/beamlinetools/BEAMLINE_CONFIG/beamline_devices" # To be changed when device configuration files are not managed/tracked by "beamlinetools" repository
RML_FILE_DIR_PATH: "{{ beamlinetools_repo_dest }}/beamlinetools/BEAMLINE_CONFIG/beamline_devices" # To be changed when device configuration files are not managed/tracked by "beamlinetools" repository
HAPPI_DB_FILE_DIR_PATH: "{{ bluesky_deployment_dir_path }}/happi"
DEV_INSTANTIATION_SETUP_FILE_DIR_PATH: "{{ beamlinetools_repo_dest }}/beamlinetools/BEAMLINE_CONFIG/beamline_devices" # To be changed when device configuration files are not managed/tracked by "beamlinetools" repository
DEV_INSTANTIATION_SETUP_FILE_NAME: "dev_instantiation_setup.yml"
```
```
_DEV_CFG_FILE_DIR_PATH: "/opt/bluesky/dev_cfg_file_dir"
_RML_FILE_DIR_PATH: "/opt/bluesky/rml_file_dir"
_HAPPI_DB_FILE_DIR_PATH: "/opt/bluesky/happi_db_file_dir"
_DEV_INSTANTIATION_SETUP_FILE_DIR_PATH: "/opt/bluesky/dev_instantiation_setup_file_dir"
```

The definitions listed above can also be found in the ~/blusky_config file (created with [ansible](https://codebase.helmholtz.cloud/hzb/ansible) to deploy bluesky), where environment variables are defined.


### Bluesky startup scripts

The file "run_bluesky.sh", which is part of project [bluesky startup scripts ](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bluesky_startup_scripts), has to contain following volume mounts and environment variable setting:


```
--env _DEV_CFG_FILE_DIR_PATH=${_DEV_CFG_FILE_DIR_PATH} \
--env _RML_FILE_DIR_PATH=${_RML_FILE_DIR_PATH} \
--env _HAPPI_DB_FILE_DIR_PATH=${_HAPPI_DB_FILE_DIR_PATH} \
--env _DEV_INSTANTIATION_SETUP_FILE_DIR_PATH=${_DEV_INSTANTIATION_SETUP_FILE_DIR_PATH} \
--env DEV_INSTANTIATION_SETUP_FILE_NAME=${DEV_INSTANTIATION_SETUP_FILE_NAME} \
```
```
-v ${DEV_CFG_FILE_DIR_PATH}:${_DEV_CFG_FILE_DIR_PATH} \
-v ${RML_FILE_DIR_PATH}:${_RML_FILE_DIR_PATH} \
-v ${HAPPI_DB_FILE_DIR_PATH}:${_HAPPI_DB_FILE_DIR_PATH} \
-v ${DEV_INSTANTIATION_SETUP_FILE_DIR_PATH}:${_DEV_INSTANTIATION_SETUP_FILE_DIR_PATH} \
```

### Bluesky container

The file "Dockerfile", which is part of project [bluesky container](https://codebase.helmholtz.cloud/hzb/bluesky/core/images/bluesky_container), has to contain following Run command:

```
RUN cd /opt/bluesky && git clone https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices_instantiation.git
RUN cd /opt/bluesky/bessyii_devices_instantiation && pip install .
```
