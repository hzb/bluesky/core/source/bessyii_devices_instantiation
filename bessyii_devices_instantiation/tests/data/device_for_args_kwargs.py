from ophyd import Device

class DeviceForTestingKwargsArgs(Device):
    """
    A subclass of ophyd.Device that represents a device with additional attributes.

    Attributes:
        str_value (str): A string parameter for the device.
        dict_value (dict): A dictionary parameter storing key-value pairs.
        float_value (float): A floating-point value representing a numerical attribute.
        int_value (int): An integer value representing a numerical attribute.
    
    Args:
        str_value (str): The string value to be assigned.
        dict_value (dict): A dictionary containing key-value pairs.
        float_value (float): The floating-point number to be assigned.
        int_value (int): The integer number to be assigned.
        *args: Additional positional arguments passed to the parent `Device` class.
        **kwargs: Additional keyword arguments passed to the parent `Device` class.
    """
    def __init__(self, str_value: str, dict_value: dict, float_value: float, int_value: int, bool_value: bool, *args, **kwargs):

        # Call the parent class (Device) constructor
        super().__init__(*args, **kwargs)
        
        # Initialize the attributes
        self.str_value = str_value
        self.dict_value = dict_value
        self.float_value = float_value
        self.int_value = int_value
        self.bool_value = bool_value
        
    def __repr__(self):
        """
        Returns a string representation of the object.

        Returns:
            str: A formatted string containing the instance attributes.
        """
        return (f"DeviceTestingPassingKwargsArgs(str_value={self.str_value}, "
                f"dict_value={self.dict_value}, float_value={self.float_value}, "
                f"int_value={self.int_value}, bool_value={self.bool_value}, name={self.name}")
