"""Module:              test_beamline_devices.py

Description:            Test functions of beamline_devices.py responsible for instantiation
                        of devices configured in device configuration y(a)ml file.

                        For this test please use device configuration file "dev_cfg_file_sim_ophyd.yml"
                        containing only simulated ophyd devices (SynAxis)

                        Following tests show that:
                            - it is possible to pass kwargs defined in the device configuration file to the constructor of a device class to be instantiated
                            - the connection to all instantiated device can be successfully established
                            - the metadata extracted from device configuration file and rml file is visible for the device instances. This is shown for m1 and m6
                            - the bluesky.plans.count executed on m1 generates messages containing objects of class SynAxis (simulated ophyd device) with expected device attributes
                            - the expected metadata is contained in docs after a run of a count with supplemental nexus data. This is shown for m5

Test data directory:    .data
Test files:             .data/dev_cfg_file_sim_ophyd.yml
                        .data/optics_trial_sim_ophyd.rml

Aux modules dir:        .aux_modules
aux modules:            .aux_modules/supplemental_data_nexus.py
"""

import os
import pytest

from typing import Generator

from bluesky.run_engine import RunEngine
from databroker.v2 import temp

from bluesky.plans import count
from bluesky.utils import Msg as plan_msg
from ophyd.sim import SynAxis
from ophyd_async.epics.sim import Sensor as ophyd_async_epics_sim_sensor

from ophyd import Device
from bessyii_devices_instantiation.tests.data.device_for_args_kwargs import DeviceForTestingKwargsArgs
from bessyii_devices_instantiation.tests.aux_modules.supplemental_data_nexus import SupplementalDataNexus
from bessyii_devices_instantiation.beamline_devices import (
    instantiate,
    wait_for_device_connection,
)


# Define path to data folder
data_folder: str = os.path.join(os.path.dirname(__file__), "data")


# Define path to database folder
database_folder: str = os.path.join(os.path.dirname(__file__), "database")


# Define path to dev_cfg_file_sim_ophyd
dev_cfg_file_sim_ophyd_name: str = "dev_cfg_file_sim_ophyd.yml"
dev_cfg_file_sim_ophyd_path: str = os.path.join(data_folder, dev_cfg_file_sim_ophyd_name)


# Define path to dev_cfg_file_sim_ophyd_async
dev_cfg_file_sim_ophyd_async_name: str = "dev_cfg_file_sim_ophyd_async.yml"
dev_cfg_file_sim_ophyd_async_path: str = os.path.join(data_folder, dev_cfg_file_sim_ophyd_async_name)


# Define path to dev_cfg_file_sim_ophyd_async
dev_cfg_file_args_kwargs_name: str = "dev_cfg_file_args_kwargs.yml"
dev_cfg_file_args_kwargs_path: str = os.path.join(data_folder, dev_cfg_file_args_kwargs_name)


# Define path to db json file that is going to be created by happi
db_file_name: str = "db.json"
db_file_path: str = os.path.join(database_folder, db_file_name)


# Define path to rml-file serving as "test data"
# The code snippet is defining a variable `rml_file_name` in Python.
rml_file_name: str = "optics_trial_sim_ophyd.rml"
rml_file_path: str = os.path.join(data_folder, rml_file_name)


# Test if "data_folder" is pointing to a folder
def test_data_dir_exist():
    assert os.path.isdir(data_folder)


# Test if "database_folder" is pointing to a folder
def test_database_dir_exist():
    assert os.path.isdir(database_folder)


# Test if "dev_cfg_file_sim_ophyd_path" is pointing to a file
def test_dev_cfg_file_sim_ophyd_exist():
    assert os.path.isfile(dev_cfg_file_sim_ophyd_path)


# Test if "dev_cfg_file_sim_ophyd_async_path" is pointing to a file
def test_dev_cfg_file_sim_ophyd_async_exist():
    assert os.path.isfile(dev_cfg_file_sim_ophyd_async_path)

# Test if "dev_cfg_file_args_kwargs_path" is pointing to a file
def test_dev_cfg_file_args_kwargs_exist():
    assert os.path.isfile(dev_cfg_file_args_kwargs_path)

# Test if "rml_file_path" is pointing to a file
def test_rml_file_exist():
    assert os.path.isfile(rml_file_path)

# Test instantiate sim ophyd devices
@pytest.fixture(scope="module")
def test_instantiate_sim_ophyd():
    devices: dict[str, SynAxis] = instantiate(
        dev_cfg_file_sim_ophyd_path, db_file_path, rml_file_path
    )

    # check type of devices
    assert isinstance(devices, dict)

    # check number of instantiated devices
    actual: int = len(devices)
    expected: int = 6
    assert actual == expected

    # check type of instantiated devices
    for obj in devices.values():
        assert isinstance(obj, SynAxis)

    # check instance names
    expected_instance_names: list[str] = ["m1", "m2", "m3", "m4", "m5", "m6"]
    for item in expected_instance_names:
        assert (item in devices.keys()) is True

    return devices


# Test instantiate sim ophyd_async devices
@pytest.fixture(scope="module")
def test_instantiate_sim_ophyd_async():
    devices: dict[str, object] = instantiate(
        dev_cfg_file_sim_ophyd_async_path, db_file_path, rml_file_path
    )

    # check type of devices
    assert isinstance(devices, dict)

    # check number of instantiated devices
    actual: int = len(devices)
    expected: int = 1
    assert actual == expected

    # check type of instantiated devices
    for obj in devices.values():
        if ("ophyd_async_sensor" == obj.name):
            assert isinstance(obj, ophyd_async_epics_sim_sensor)

    # check instance names
    expected_instance_names: list[str] = ["ophyd_async_sensor"]
    for item in expected_instance_names:
        assert (item in devices.keys()) is True

    return devices


# Test instantiate DeviceForTestingKwargsArgs devices
@pytest.fixture(scope="module")
def test_instantiate_device_testing_passing_kwargs_args():
    devices: dict[str, DeviceForTestingKwargsArgs] = instantiate(
        dev_cfg_file_args_kwargs_path, db_file_path, rml_file_path
    )

    # check type of devices
    assert isinstance(devices, dict)

    # check number of instantiated devices
    actual: int = len(devices)
    expected: int = 3
    assert actual == expected

    # check type of instantiated devices
    for obj in devices.values():
        assert isinstance(obj, DeviceForTestingKwargsArgs)

    # check instance names
    expected_instance_names: list[str] = ["dev_1", "dev_2", "dev_3"]
    for item in expected_instance_names:
        assert (item in devices.keys()) is True

    return devices

# Test wait for sim ophyd devices
def test_wait_for_device_connection_sim_ophyd(test_instantiate_sim_ophyd):
    devices: dict[str, SynAxis] = test_instantiate_sim_ophyd

    # Wait for device connection
    wait_for_device_connection(devices)

    # check if all devices connected
    for obj in devices.values():
        assert obj.connected is True

# Test wait for sim ophyd_async devices
def test_wait_for_device_connection_sim_ophyd_async(test_instantiate_sim_ophyd_async):
    devices: dict[str, object] = test_instantiate_sim_ophyd_async

    # Create instance of RunEngine, necessary for connecting to ophyd-async device
    run_engine: RunEngine = RunEngine()

    # Wait for device connection
    wait_for_device_connection(devices, run_engine)

    # check if all devices connected
    for obj in devices.values():
        assert obj.connected is True
        
    # Test of rising an exception when invalid run_engine
    with pytest.raises(ValueError):
        # Wait for device connection
        run_engine = None
        wait_for_device_connection(devices, run_engine)


# Test wait for device_testing_passing_kwargs_args
def test_wait_for_device_testing_passing_kwargs_args(test_instantiate_device_testing_passing_kwargs_args):
    devices: dict[str, DeviceForTestingKwargsArgs] = test_instantiate_device_testing_passing_kwargs_args

    # # Wait for device connection
    wait_for_device_connection(devices)

    # check if all devices connected
    for obj in devices.values():
        assert obj.connected is True


# Test dev_1
def test_dev_1(test_instantiate_device_testing_passing_kwargs_args):
    devices: dict[str, DeviceForTestingKwargsArgs] = test_instantiate_device_testing_passing_kwargs_args

    dev_1: DeviceForTestingKwargsArgs = devices["dev_1"]

    # ------------ Define expectations for dev_1
    expected_name: str = "dev_1"
    expected_prefix: str = "A:"
    expected_str_value: str = "arg1Val"
    expected_dict_value: dict = {"a": 23}
    expected_float_value: float = 3.1415
    expected_int_value: int = 9
    expected_bool_value: bool = False
    # ------------ End of Define expectations for dev_1

    # ------------ Assertions for dev_1
    assert dev_1.name == expected_name
    assert dev_1.prefix == expected_prefix
    assert dev_1.str_value == expected_str_value
    assert dev_1.dict_value == expected_dict_value
    assert dev_1.float_value == expected_float_value
    assert dev_1.int_value == expected_int_value
    assert dev_1.bool_value == expected_bool_value
    # ------------ End of Assertions for dev_1

# Test dev_2
def test_dev_2(test_instantiate_device_testing_passing_kwargs_args):
    devices: dict[str, DeviceForTestingKwargsArgs] = test_instantiate_device_testing_passing_kwargs_args

    dev_2: DeviceForTestingKwargsArgs = devices["dev_2"]

    # ------------ Define expectations for dev_2
    expected_name: str = "dev_2"
    expected_prefix: str = "B:"
    expected_str_value: str = "dt"
    expected_dict_value: dict = {"s": 56.7}
    expected_float_value: float = 3.1415
    expected_int_value: int = 14
    expected_bool_value: bool = True
    # ------------ End of Define expectations for dev_2

    # ------------ Assertions for dev_2
    assert dev_2.name == expected_name
    assert dev_2.prefix == expected_prefix
    assert dev_2.str_value == expected_str_value
    assert dev_2.dict_value == expected_dict_value
    assert dev_2.float_value == expected_float_value
    assert dev_2.int_value == expected_int_value
    assert dev_2.bool_value == expected_bool_value
    # ------------ End of Assertions for dev_2

# Test dev_3
def test_dev_3(test_instantiate_device_testing_passing_kwargs_args):
    devices: dict[str, DeviceForTestingKwargsArgs] = test_instantiate_device_testing_passing_kwargs_args

    dev_3: DeviceForTestingKwargsArgs = devices["dev_3"]

    # ------------ Define expectations for dev_3
    expected_name: str = "dev_3"
    expected_prefix: str = "C:"
    expected_str_value: str = "arg5Val"
    expected_dict_value: dict = {"a": 23}
    expected_float_value: float = 3.141
    expected_int_value: int = 17
    expected_bool_value: bool = True
    # ------------ End of Define expectations for dev_3

    # ------------ Assertions for dev_3
    assert dev_3.name == expected_name
    assert dev_3.prefix == expected_prefix
    assert dev_3.str_value == expected_str_value
    assert dev_3.dict_value == expected_dict_value
    assert dev_3.float_value == expected_float_value
    assert dev_3.int_value == expected_int_value
    assert dev_3.bool_value == expected_bool_value
    # ------------ End of Assertions for dev_3


# Test m1 which is a "Plane Mirror" according to rml-file. Fetching metadata from rml-file is "ON" (apply_rml_on: True)
def test_m1(test_instantiate_sim_ophyd):
    devices: dict[str, SynAxis] = test_instantiate_sim_ophyd

    m1: SynAxis = devices["m1"]

    # ------------ Define expectations for m1

    # Expected kwargs
    expected_name: str = "m1"
    expected_prefix: str = ""
    expected_delay: int = 3
    expected_precision: float = 1.23
    expected_kind_value: int = 7
    expected_labels: set[str] = set(["motors"])
    expected_read_attrs: list[str] = ["readback", "setpoint", "acceleration"]
    expected_configuration_attrs: list[str] = [
        "acceleration",
        "readback",
        "setpoint",
        "velocity",
    ]
    # Expected metadata from device configuration file
    expected_desc: str = "Description for m1"
    # Expected metadata from rml file
    expected_thicknessCoating1: str = "1.111"
    expected_materialCoating1: str = "ab"
    expected_worldPosition: dict = {
        "x": "1.1000000000000001",
        "y": "2.1000000000000001",
        "z": "3.1000000000000001",
    }
    # ------------ End of Define expectations for m1

    # ------------ Assertions for m1

    # Assert kwargs
    assert m1.name == expected_name
    assert m1.prefix == expected_prefix
    assert m1.delay == expected_delay
    assert m1.precision == expected_precision
    assert m1.kind.value == expected_kind_value
    assert m1._ophyd_labels_ == expected_labels
    assert sorted(m1.read_attrs) == sorted(expected_read_attrs)
    assert sorted(m1.configuration_attrs) == sorted(expected_configuration_attrs)
    # Assert metatdata from device configuration file
    assert m1.md.desc == expected_desc
    # Assert metadata from rml file. It is fetched from rml for m1 since apply_rml_on: True
    assert m1.md.thicknessCoating1 == expected_thicknessCoating1
    assert m1.md.materialCoating1 == expected_materialCoating1
    assert m1.md.worldPosition == expected_worldPosition
    # ------------ End of Assertions for m1


# Test m6 which is a "Slit" according to rml-file. Fetching metadata from rml-file is "OFF" (apply_rml_on: False)
def test_m6(test_instantiate_sim_ophyd):
    devices: dict[str, SynAxis] = test_instantiate_sim_ophyd

    m6: SynAxis = devices["m6"]

    # ------------ Define expectations for m6

    # Expected kwargs
    expected_name: str = "m6"
    expected_prefix: str = ""
    expected_read_attrs: list[str] = ["readback", "setpoint", "acceleration"]

    # Expected metadata from device configuration file for m6
    expected_desc: str = "Description for m6"
    # Expected metadata from rml file for m6
    expected_worldPosition: dict = {
        "x": "-1.1216006",
        "y": "-2.2326006",
        "z": "-3.3436006",
    }
    # ------------ End of Define expectations for m6

    # ------------ Assertions for m6

    # Assert kwargs
    assert m6.name == expected_name
    assert m6.prefix == expected_prefix
    assert sorted(m6.read_attrs) == sorted(expected_read_attrs)
    # Assert metatdata from device configuration file
    assert m6.md.desc == expected_desc
    # Assert NO metadata from rml file. It is not fetched for m6 (from rml file) since apply_rml_on: False
    with pytest.raises(AttributeError):
        assert m6.md.worldPosition == expected_worldPosition
    # ------------ End of Assertions for m6


# Test bluesky.plans.count plan applied on m1
def test_m1_count_plan(test_instantiate_sim_ophyd):
    devices: dict[str, SynAxis] = test_instantiate_sim_ophyd

    m1: SynAxis = devices["m1"]

    # Create a plan for m1
    plan: Generator[plan_msg] = count([m1], 2)

    # Collect from plan all messages (plan_msg) for which: type(obj) is SynAxis
    messages: list[plan_msg] = [item for item in plan if type(item.obj) is SynAxis]

    # There are 6 messages (plan_msg) for which  type(obj) is SynAxis.
    # This is a case when value of "command": [stage, trigger, read, trigger, read, unstage]
    expected_messages_len: int = 6
    assert len(messages) == expected_messages_len

    # ------------Define all expectations
    expected_prefix: str = ""
    expeced_name: str = "m1"
    expected_read_attrs: list[str] = ["readback", "setpoint", "acceleration"]
    expected_configuration_attrs: list[str] = [
        "readback",
        "setpoint",
        "velocity",
        "acceleration",
    ]
    # ------------End of Define all expectations

    # ------------Assertions (for all messages)
    for msg in messages:
        assert isinstance(msg.obj, SynAxis)
        assert msg.obj.prefix == expected_prefix
        assert msg.obj.name == expeced_name
        assert sorted(msg.obj.read_attrs) == sorted(expected_read_attrs)
        assert sorted(msg.obj.configuration_attrs) == sorted(
            expected_configuration_attrs
        )
    # ------------End of Assertions


# Test of a metadata contained in docs after a run of a count with supplemental nexus data
def test_run_of_count_with_supplemental_nexus_data(test_instantiate_sim_ophyd):
    devices: dict[str, SynAxis] = test_instantiate_sim_ophyd

    m5: SynAxis = devices["m5"]

    RE = RunEngine({})
    db = temp()
    RE.subscribe(db.v1.insert)

    sdn = SupplementalDataNexus()
    RE.preprocessors.append(sdn)

    # Add the elements that we want to monitor to the nexus_list
    sdn.nexus_list = [m5]

    # # # make a count
    RE(count([m5], 2))

    # Expectation for world_position defined in rml file for device m5
    expected_world_postion_x: str = "-1.1215005"
    expected_world_postion_y: str = "-2.2325005"
    expected_world_postion_z: str = "-3.3435005"
    expected_description: str = "Description for m5"

    run = db[-1]

    for key in run.metadata.keys():
        try:
            for k in run.metadata[key].keys():
                if k == "nexus_md":
                    nmd = run.metadata[key][k]
                    for k_nmd in nmd.keys():
                        # Filter out "worldPosition"
                        if nmd[k_nmd]["worldPosition"]:
                            assert (
                                nmd[k_nmd]["worldPosition"]["x"]
                                == expected_world_postion_x
                            )
                            assert (
                                nmd[k_nmd]["worldPosition"]["y"]
                                == expected_world_postion_y
                            )
                            assert (
                                nmd[k_nmd]["worldPosition"]["z"]
                                == expected_world_postion_z
                            )

                        # Filter out "desc"
                        if nmd[k_nmd]["desc"]:
                            assert nmd[k_nmd]["desc"] == expected_description
                        pass
        except AttributeError:
            pass


# Only for debugging the instantiation process
def debug_of_instantiate_sim_ophyd():
    devices: dict[str, SynAxis] = instantiate(
        dev_cfg_file_sim_ophyd_path, db_file_path, rml_file_path
    )

    # check type of devices
    assert isinstance(devices, dict)

    # check number of instantiated devices
    actual: int = len(devices)
    expected: int = 6
    assert actual == expected

    # check type of instantiated devices
    for obj in devices.values():
        assert isinstance(obj, SynAxis)

    # check instance names
    expected_instance_names: list[str] = ["m1", "m2", "m3", "m4", "m5", "m6"]
    for item in expected_instance_names:
        assert (item in devices.keys()) is True

    return devices


def debug_instantiate_device_testing_passing_kwargs_args():
    devices: dict[str, DeviceForTestingKwargsArgs] = instantiate(
        dev_cfg_file_args_kwargs_path, db_file_path, rml_file_path
    )

    # check type of devices
    assert isinstance(devices, dict)

    # check number of instantiated devices
    actual: int = len(devices)
    expected: int = 3
    assert actual == expected

    # check type of instantiated devices
    for obj in devices.values():
        assert isinstance(obj, DeviceForTestingKwargsArgs)

    # check instance names
    expected_instance_names: list[str] = ["dev_1", "dev_2", "dev_3"]
    for item in expected_instance_names:
        assert (item in devices.keys()) is True
    return devices


def debug_wait_for_device_connection(devices: dict):

    # Wait for device connection
    wait_for_device_connection(devices)

    # check if all devices connected
    for obj in devices.values():
        assert obj.connected is True

if __name__ == "__main__":
    # debug_of_instantiate_sim_ophyd()
    devices: dict = debug_instantiate_device_testing_passing_kwargs_args()
    debug_wait_for_device_connection(devices)
