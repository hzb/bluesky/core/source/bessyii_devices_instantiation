"""Module:              test_beamline_devices_cfg.py

Description:            Test functions of beamline_devices_cfg.py


Actions:

                        Test function:
                            - verify_dev_cfg()

Test data directory:    .data
Test file:              .data/dev_cfg_file_errored_0.yml
                        .data/dev_cfg_file_errored_1.yml
                        .data/dev_cfg_file_errored_2.yml
"""

import os
import pytest

from typing import Generator

from bessyii_devices_instantiation.beamline_devices_cfg import verify_dev_cfg
from bessyii_devices_instantiation.common.yaml_utils import read_yaml

# Define path to data folder
data_folder: str = os.path.join(os.path.dirname(__file__), "data")

# Define path to dev cfg-file "dev_cfg_file_errored_0.yml" where some mandatory keys are missing serving as "test data"
dev_cfg_file_name: str = "dev_cfg_file_errored_0.yml"
dev_cfg_file_errored_0_path: str = os.path.join(data_folder, dev_cfg_file_name)

# Define path to dev cfg-file "dev_cfg_file_errored_1.yml" where some values are not unique serving as "test data"
dev_cfg_file_name: str = "dev_cfg_file_errored_1.yml"
dev_cfg_file_errored_1_path: str = os.path.join(data_folder, dev_cfg_file_name)

# Define path to dev cfg-file "dev_cfg_file_errored_2.yml" which is empty serving as "test data"
dev_cfg_file_name: str = "dev_cfg_file_errored_2.yml"
dev_cfg_file_errored_2_path: str = os.path.join(data_folder, dev_cfg_file_name)


# Test if "data_folder" is pointing to a folder
def test_data_dir_exist():
    assert os.path.isdir(data_folder)


# Test if each dev cfg file is pointing to a file
def test_dev_cfg_file_exist():
    assert os.path.isfile(dev_cfg_file_errored_0_path)
    assert os.path.isfile(dev_cfg_file_errored_1_path)
    assert os.path.isfile(dev_cfg_file_errored_2_path)


# Setup for the test: Read dev cfg y(a)ml files
@pytest.fixture(scope="module")
def setup() -> Generator:
    # Read from "dev_cfg_file_errored_0_path"
    dev_cfg_list_errored_0: list[dict] | dict | None = read_yaml(
        dev_cfg_file_errored_0_path
    )
    # Read from "dev_cfg_file_errored_1_path"
    dev_cfg_list_errored_1: list[dict] | dict | None = read_yaml(
        dev_cfg_file_errored_1_path
    )
    # Read from "dev_cfg_file_errored_2_path"
    dev_cfg_list_errored_2: list[dict] | dict | None = read_yaml(
        dev_cfg_file_errored_2_path
    )

    yield dev_cfg_list_errored_0, dev_cfg_list_errored_1, dev_cfg_list_errored_2


# Test verification of device configuration
def test_verify_dev_cfg(setup, capsys):
    # "dev_cfg_file" where mandatory keys are missing
    with pytest.raises(NameError):
        dev_cfg_list: list[dict] | dict | None = setup[0]
        verify_dev_cfg(dev_cfg_list)

    # "dev_cfg_file" where some keys are not unique
    dev_cfg_list: list[dict] | dict | None = setup[1]
    verify_dev_cfg(dev_cfg_list)
    captured = capsys.readouterr()
    expected_readout = (
        "device config yml file are not unique. That could be intentional"
    )
    assert expected_readout in captured.out

    # "dev_cfg_file" is empty
    with pytest.raises(ValueError):
        dev_cfg_list: list[dict] | dict | None = setup[2]
        verify_dev_cfg(dev_cfg_list)
