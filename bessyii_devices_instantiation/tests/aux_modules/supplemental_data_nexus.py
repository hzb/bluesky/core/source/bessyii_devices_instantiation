from bluesky.preprocessors import SupplementalData
from bluesky.preprocessors import inject_md_wrapper


def populate_nexus_dict(nexus_list):
    """Populate the nexus dictionary by reading the motor positions/det values.

    Args:
        nexus_list (_type_): The list of element that should be updated

    Returns:
        dict: the nexus dictionary to be added to the metadata
    """
    nexus_md = {}
    nexus_md["nexus_md"] = {}
    for ob in nexus_list:
        nexus_md["nexus_md"][ob.name] = ob.md.get_attributes()
    return nexus_md


class SupplementalDataNexus(SupplementalData):
    """Supplemental data for Nexus.

    Usage:
    sdn = SupplementalDataNexus()
    RE.preprocessors.append(sdn)
    sdn.nexus_list = [<list of oe to be included in nexus>]


    Args:
            self.nexus_list (list): list of oe to be included in nexus
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __call__(self, plan):
        nexus_md = populate_nexus_dict(self.nexus_list)
        plan = inject_md_wrapper(plan, nexus_md)
        return (yield from plan)
