"""Module:              test_beamline_devices_rml.py

Description:            Test functions from beamline_devices_rml.py

Actions:
                        Test function:
                            - get_md_from_rml()

Test data directory:    .data
Test file:              .data/optics_trial.rml
"""

import os
import pytest

from bessyii_devices_instantiation.beamline_devices_rml import get_md_from_rml
from bessyii_devices_instantiation.beamline_devices_const import DeviceRmlMetaData

# Define path to data folder
data_folder: str = os.path.join(os.path.dirname(__file__), "data")

# Define path to rml-file serving as "test data"
file_name: str = "optics_trial.rml"
file_path: str = os.path.join(data_folder, file_name)


# Test if "data_folder" is pointing to a folder
def test_data_dir():
    assert os.path.isdir(data_folder)


# Test if "file_path" is pointing to a file
def test_data_file():
    assert os.path.isfile(file_path)


# Test get metadata from rml file for given device configuration and rml file
def test_get_md_from_rml():
    # Define a list corresponding to content of device configuration y(a)ml file
    dev_cfg_list: list[dict] = [
        {
            "type": "I am a value of type",
            "active": True,
            "class": "I am a value of class",
            "prefix": "I am a value of prefix:",
            "instance_name": "v1",  # I am defined in rml test file as a "Plane Mirror"
            "apply_rml_on": True,  # I allow to extract metadata from rml file
            "desc11": "I am an arbitrary metadata",
        },
        {
            "type": "I am a value of type",
            "active": True,
            "class": "I am a value of class",
            "prefix": "I am a value of prefix:",
            "instance_name": "v2",  # I am defined in rml test file as a "Plane Mirror"
            "apply_rml_on": False,  # I do not allow to extract metadata from rml file
            "desc22": "I am an arbitrary metadata",
        },
        {
            "type": "I am a value of type",
            "active": False,
            "class": "I am a value of class",
            "prefix": "I am a value of prefix:",
            "instance_name": "v3",  # I am defined in rml test file as a "Plane Mirror"
            "apply_rml_on": True,  # I allow to extract metadata from rml file
            "desc33": "I am an arbitrary metadata",
        },
        {
            "type": "I am a value of type",
            "active": False,
            "class": "I am a value of class",
            "prefix": "I am a value of prefix:",
            "instance_name": "v4",  # I am defined in rml test file as a "Plane Mirror"
            "apply_rml_on": False,  # I do not allow to extract metadata from rml file
            "desc44": "I am an arbitrary metadata",
        },
        {
            "type": "I am a value of type",
            "active": True,
            "class": "I am a value of class",
            "prefix": "I am a value of prefix:",
            "instance_name": "v5",  # I am defined in rml test file as a "Slit"
            "apply_rml_on": True,  # I allow to extract metadata from rml file
            "desc55": "I am an arbitrary metadata",
        },
        {
            "type": "I am a value of type",
            "active": True,
            "class": "I am a value of class",
            "prefix": "I am a value of prefix:",
            "instance_name": "v6",  # I am defined in rml test file as a "Slit"
            "apply_rml_on": False,  # I do not allow  to extract metadata from rml file
            "desc66": "I am an arbitrary metadata",
        },
        {
            "type": "I am a value of type",
            "active": False,
            "class": "I am a value of class",
            "prefix": "I am a value of prefix:",
            "instance_name": "v7",  # I am defined in rml test file as a "Slit"
            "apply_rml_on": True,  # I allow to extract metadata from rml file
            "desc77": "I am an arbitrary metadata",
        },
        {
            "type": "I am a value of type",
            "active": False,
            "class": "I am a value of class",
            "prefix": "I am a value of prefix:",
            "instance_name": "v8",  # I am defined in rml test file as a "Slit"
            "apply_rml_on": False,  # I do not allow to extract metadata from rml file
            "desc88": "I am an arbitrary metadata",
        },
    ]

    # Define expected result from the function under test "get_md_from_rml" for given inputs: "dev_cfg_list" and "file_path" of rml test file
    expected: dict[str, DeviceRmlMetaData] = {
        # v1 is defined as a "Plane Mirror"
        "v1": {
            "materialCoating1": "ab",
            "thicknessCoating1": "1.111",
            "worldPosition": {
                "x": "1.1000000000000001",
                "y": "2.1000000000000001",
                "z": "3.1000000000000001",
            },
        },
        # v3 is defined as a "Plane Mirror"
        "v3": {
            "materialCoating1": "ef",
            "thicknessCoating1": "3.333",
            "worldPosition": {
                "x": "1.3000000000000003",
                "y": "2.3000000000000003",
                "z": "3.3000000000000003",
            },
        },
        # v5 is defined as a "Slit"
        "v5": {
            "worldPosition": {
                "x": "-1.1215005",
                "y": "-2.2325005",
                "z": "-3.3435005",
            },
        },
        # v7 is defined as a "Slit"
        "v7": {
            "worldPosition": {
                "x": "-1.1217007",
                "y": "-2.2327007",
                "z": "-3.3437007",
            },
        },
    }
    # Call function under test
    actual: dict[str, DeviceRmlMetaData] = get_md_from_rml(file_path, dev_cfg_list)
    assert actual == expected
