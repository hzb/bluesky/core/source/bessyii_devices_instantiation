"""Module:              test_beamline_devices_happi.py

Description:            Test functions from beamline_devices_happi.py

Actions:
                        Test function:
                            - create_containers()

Test data directory:    .data
Test file:              .data/optics_trial.rml
"""

import os
import pytest

from bessyii_devices_instantiation.beamline_devices_happi import (
    create_containers,
    DevCon,
)
from bessyii_devices_instantiation.beamline_devices_rml import get_md_from_rml
from bessyii_devices_instantiation.beamline_devices_const import DeviceRmlMetaData
from bessyii_devices_instantiation.common.yaml_utils import read_yaml

# Define path to data folder
data_folder: str = os.path.join(os.path.dirname(__file__), "data")

# Define path to rml-file serving as "test data"
rml_file_name: str = "optics_trial.rml"
rml_file_path: str = os.path.join(data_folder, rml_file_name)


# Define path to dev cfg-file serving as "test data"
dev_cfg_file_name: str = "dev_cfg_file.yml"
dev_cfg_file_path: str = os.path.join(data_folder, dev_cfg_file_name)


# Test if "data_folder" is pointing to a folder
def test_data_dir_exist():
    assert os.path.isdir(data_folder)


# Test if "rml_file_path" is pointing to a file
def test_rml_file_exist():
    assert os.path.isfile(rml_file_path)


# Test if "dev_cfg_file_path" is pointing to a file
def test_dev_cfg_file_exist():
    assert os.path.isfile(dev_cfg_file_path)


# Setup for the test of function: "create_containers()""
@pytest.fixture(scope="module")
def setup():
    # Read from device configuration y(a)ml file
    DEV_CFG_LIST: list[dict] | dict | None = read_yaml(dev_cfg_file_path)

    # Extract metadata from rml file for given device config list
    RML_METADATA: dict[str, DeviceRmlMetaData] = get_md_from_rml(
        rml_file_path, DEV_CFG_LIST
    )

    yield DEV_CFG_LIST, RML_METADATA


# Test of create containers for given device configuration list and metadata from rml file
def test_create_containers(setup):
    dev_cfg_list: list[dict] | dict | None = setup[0]
    rml_metadata: dict[str, DeviceRmlMetaData] = setup[1]

    # Check for how many devices metadata was extracted from rml file. Instance names of devices allowing
    # extraction of metadata from rml file are: "v1", "v3", "v5". ("apply_rml_on": True)
    expected = 3
    actual = len(rml_metadata.items())
    assert expected == actual

    # Call function under test
    containers: list[DevCon] = create_containers(dev_cfg_list, rml_metadata)

    # Check total number of created containers for devices defined in dev_cfg_list
    expected = 5
    actual = len(containers)
    assert expected == actual

    ###
    ### Check values of attibutes in container created for device with instance name: "v1" which is a "Plane Mirror" acccording to rml file
    ###
    index = 0
    # ------------------------------------ From dev config y(a)ml file ----------------------------
    expected = "Plane Mirror 1"
    actual = containers[index].type
    assert expected == actual

    expected = True
    actual = containers[index].active
    assert expected == actual

    expected = "bessyii_devices.valve.PositionerBessyValve_1"
    actual = containers[index].device_class
    assert expected == actual

    expected = "V01V:"
    actual = containers[index].kwargs["prefix"]
    assert expected == actual

    expected = "v1"
    actual = containers[index].name
    assert expected == actual

    expected = "v1"
    actual = containers[index].kwargs["name"]
    assert expected == actual

    expected = ["readback", "reset"]
    actual = containers[index].kwargs["read_attrs"]
    assert expected == actual

    expected = ["detectors"]
    actual = containers[index].kwargs["labels"]
    assert expected == actual

    expected = "Arbitrary metadata 1_1"
    actual = containers[index].matadata_1_1
    assert expected == actual

    expected = "Arbitrary metadata 1_2"
    actual = containers[index].matadata_1_2
    assert expected == actual

    expected = "Arbitrary metadata 1_10"
    actual = containers[index].matadata_1_10
    assert expected == actual

    with pytest.raises(AttributeError):
        expected = "Arbitrary metadata 1_11"
        actual = containers[index].matadata_1_11
        assert expected == actual

    # ------------------------------------ From rml file ("v1" allows extraction of metadata from rml-file) ----------------------------
    expected = "ab"
    actual = containers[index].materialCoating1
    assert expected == actual

    expected = "1.111"
    actual = containers[index].thicknessCoating1
    assert expected == actual

    expected = "1.1000000000000001"
    actual = containers[index].worldPosition["x"]
    assert expected == actual

    expected = "2.1000000000000001"
    actual = containers[index].worldPosition["y"]
    assert expected == actual

    expected = "3.1000000000000001"
    actual = containers[index].worldPosition["z"]
    assert expected == actual

    with pytest.raises(AttributeError):
        expected = "0"
        actual = containers[index].densityCoating1
        assert expected == actual

    ###
    ### Check values of attibutes in container created for device with instance name: "v2" which is a "Plane Mirror" acccording to rml file
    ###
    index = 1
    # ------------------------------------ From dev config y(a)ml file ----------------------------
    expected = "Plane Mirror 2"
    actual = containers[index].type
    assert expected == actual

    expected = True
    actual = containers[index].active
    assert expected == actual

    expected = "bessyii_devices.valve.PositionerBessyValve_2"
    actual = containers[index].device_class
    assert expected == actual

    expected = "V02V:"
    actual = containers[index].kwargs["prefix"]
    assert expected == actual

    expected = "v2"
    actual = containers[index].name
    assert expected == actual

    expected = "v2"
    actual = containers[index].kwargs["name"]
    assert expected == actual

    expected = "Arbitrary metadata 2_1"
    actual = containers[index].matadata_2_1
    assert expected == actual

    expected = "Arbitrary metadata 2_2"
    actual = containers[index].matadata_2_2
    assert expected == actual

    expected = "Arbitrary metadata 2_10"
    actual = containers[index].matadata_2_10
    assert expected == actual

    with pytest.raises(AttributeError):
        expected = "Arbitrary metadata 2_11"
        actual = containers[index].matadata_2_11
        assert expected == actual

    # ------------------------------------ From rml file ("v2" does not allow extraction of metadata from rml-file) ----------------------------
    with pytest.raises(AttributeError):
        expected = "cd"
        actual = containers[index].materialCoating1
        assert expected == actual
    with pytest.raises(AttributeError):
        expected = "2.222"
        actual = containers[index].thicknessCoating1
        assert expected == actual

    with pytest.raises(AttributeError):
        expected = "1.2000000000000002"
        actual = containers[index].worldPosition["x"]
        assert expected == actual
    with pytest.raises(AttributeError):
        expected = "2.2000000000000002"
        actual = containers[index].worldPosition["y"]
        assert expected == actual
    with pytest.raises(AttributeError):
        expected = "3.2000000000000002"
        actual = containers[index].worldPosition["z"]
        assert expected == actual

    ###
    ### Check values of attibutes in container created for device with instance name: "v3" which is a "Plane Mirror" acccording to rml file
    ###
    index = 2
    # ------------------------------------ From dev config y(a)ml file ----------------------------
    expected = "Plane Mirror 3"
    actual = containers[index].type
    assert expected == actual

    expected = False
    actual = containers[index].active
    assert expected == actual

    expected = "bessyii_devices.valve.PositionerBessyValve_3"
    actual = containers[index].device_class
    assert expected == actual

    expected = "V03V:"
    actual = containers[index].kwargs["prefix"]
    assert expected == actual

    expected = "v3"
    actual = containers[index].name
    assert expected == actual

    expected = "v3"
    actual = containers[index].kwargs["name"]
    assert expected == actual

    expected = ["readback", "reset"]
    actual = containers[index].kwargs["read_attrs"]
    assert expected == actual

    expected = ["detectors"]
    actual = containers[index].kwargs["labels"]
    assert expected == actual

    expected = "Arbitrary metadata 3_1"
    actual = containers[index].matadata_3_1
    assert expected == actual

    expected = "Arbitrary metadata 3_2"
    actual = containers[index].matadata_3_2
    assert expected == actual

    expected = "Arbitrary metadata 3_10"
    actual = containers[index].matadata_3_10
    assert expected == actual

    with pytest.raises(AttributeError):
        expected = "Arbitrary metadata 3_11"
        actual = containers[index].matadata_3_11
        assert expected == actual

    # ------------------------------------ From rml file ("v3" allows extraction of metadata from rml-file) ----------------------------
    expected = "ef"
    actual = containers[index].materialCoating1
    assert expected == actual

    expected = "3.333"
    actual = containers[index].thicknessCoating1
    assert expected == actual

    expected = "1.3000000000000003"
    actual = containers[index].worldPosition["x"]
    assert expected == actual

    expected = "2.3000000000000003"
    actual = containers[index].worldPosition["y"]
    assert expected == actual

    expected = "3.3000000000000003"
    actual = containers[index].worldPosition["z"]
    assert expected == actual

    with pytest.raises(AttributeError):
        expected = "0"
        actual = containers[index].densityCoating1
        assert expected == actual

    ###
    ### Check values of attibutes in container created for device with instance name: "v4" which is a "Plane Mirror" acccording to rml file
    ###
    index = 3
    # ------------------------------------ From dev config y(a)ml file ----------------------------
    expected = "Plane Mirror 4"
    actual = containers[index].type
    assert expected == actual

    expected = False
    actual = containers[index].active
    assert expected == actual

    expected = "bessyii_devices.valve.PositionerBessyValve_4"
    actual = containers[index].device_class
    assert expected == actual

    expected = "V04V:"
    actual = containers[index].kwargs["prefix"]
    assert expected == actual

    expected = "v4"
    actual = containers[index].name
    assert expected == actual

    expected = "v4"
    actual = containers[index].kwargs["name"]
    assert expected == actual

    expected = "Arbitrary metadata 4_1"
    actual = containers[index].matadata_4_1
    assert expected == actual

    expected = "Arbitrary metadata 4_2"
    actual = containers[index].matadata_4_2
    assert expected == actual

    expected = "Arbitrary metadata 4_10"
    actual = containers[index].matadata_4_10
    assert expected == actual

    with pytest.raises(AttributeError):
        expected = "Arbitrary metadata 4_11"
        actual = containers[index].matadata_4_11
        assert expected == actual

    # ------------------------------------ From rml file ("v4" does not allow extraction of metadata from rml-file) ----------------------------
    with pytest.raises(AttributeError):
        expected = "gh"
        actual = containers[index].materialCoating1
        assert expected == actual
    with pytest.raises(AttributeError):
        expected = "4.444"
        actual = containers[index].thicknessCoating1
        assert expected == actual
    with pytest.raises(AttributeError):
        expected = "1.4000000000000004"
        actual = containers[index].worldPosition["x"]
        assert expected == actual
    with pytest.raises(AttributeError):
        expected = "2.4000000000000004"
        actual = containers[index].worldPosition["y"]
        assert expected == actual
    with pytest.raises(AttributeError):
        expected = "3.4000000000000004"
        actual = containers[index].worldPosition["z"]
        assert expected == actual

    ###
    ### Check values of attibutes in container created for device with instance name: "v5" which is a "Slit" acccording to rml file
    ###
    index = 4
    # ------------------------------------ From dev config y(a)ml file ----------------------------
    expected = "Slit 1"
    actual = containers[index].type
    assert expected == actual

    expected = True
    actual = containers[index].active
    assert expected == actual

    expected = "bessyii_devices.valve.PositionerBessyValve_5"
    actual = containers[index].device_class
    assert expected == actual

    expected = "V05V:"
    actual = containers[index].kwargs["prefix"]
    assert expected == actual

    expected = "v5"
    actual = containers[index].name
    assert expected == actual

    expected = "v5"
    actual = containers[index].kwargs["name"]
    assert expected == actual

    expected = "Arbitrary metadata 5_1"
    actual = containers[index].matadata_5_1
    assert expected == actual

    expected = "Arbitrary metadata 5_2"
    actual = containers[index].matadata_5_2
    assert expected == actual

    expected = "Arbitrary metadata 5_10"
    actual = containers[index].matadata_5_10
    assert expected == actual

    with pytest.raises(AttributeError):
        expected = "Arbitrary metadata 5_11"
        actual = containers[index].matadata_5_11
        assert expected == actual

    # ------------------------------------ From rml file ("v5" allows extraction of metadata from rml-file) ----------------------------
    expected = "-1.1215005"
    actual = containers[index].worldPosition["x"]
    assert expected == actual

    expected = "-2.2325005"
    actual = containers[index].worldPosition["y"]
    assert expected == actual

    expected = "-3.3435005"
    actual = containers[index].worldPosition["z"]
    assert expected == actual

    with pytest.raises(AttributeError):
        expected = "ij"
        actual = containers[index].materialCoating1
        assert expected == actual
    with pytest.raises(AttributeError):
        expected = "5.555"
        actual = containers[index].thicknessCoating1
        assert expected == actual

    with pytest.raises(AttributeError):
        expected = "0.05"
        actual = containers[index].totalHeight
        assert expected == actual
