"""Module:              test_happi_utils.py

Description:            Test functions from happi_utils.py

Actions:
                        Test function:
                            - create_json_db_backend()
                            - get_db_client()
                            - get_json_db_backend()
                            - get_db_client_from_backend()
                            - get_container_from_db()
                            - get_device_from_container()

Data base directory:    .database

"""

import os
import pytest

from happi import Client, EntryInfo, HappiItem
from happi.backends.json_db import JSONBackend

from bessyii_devices_instantiation.common.happi_utils import (
    add_to_registry,
    create_json_db_backend,
    get_container_from_db,
    get_db_client,
    get_db_client_from_backend,
    get_device_from_container,
    get_json_db_backend,
)

# Define path to database folder
database_folder: str = os.path.join(os.path.dirname(__file__), "database")

# Define path to db json file that is going to be created by happi
db_file_name: str = "db.json"
db_file_path: str = os.path.join(database_folder, db_file_name)


# Test if "database_folder" is pointing to a folder
def test_database_dir():
    assert os.path.isdir(database_folder)


# Remove file: A repetative action across many of the following test functions
@pytest.fixture(scope="module")
def remove_file():
    def _remove_file(file_path):
        if os.path.exists(file_path):
            os.remove(file_path)
        assert not os.path.isfile(file_path)

    return _remove_file


# Test creation of database JSONBackend
def test_create_json_db_backend(remove_file):
    # Remove db json file before creating a new one with the same name
    remove_file(db_file_path)

    create_json_db_backend(db_file_path)
    assert os.path.isfile(db_file_path)


# Test doubled creation of database JSONBackend
def test_doubled_create_json_db_backend(remove_file):
    # Remove db json file before creating a new one with the same name
    remove_file(db_file_path)

    # 1st creation
    create_json_db_backend(db_file_path)
    assert os.path.isfile(db_file_path)

    # 2nd creation
    with pytest.raises(PermissionError):
        create_json_db_backend(db_file_path)


# Test wrong creation of database JSONBackend
def test_wrong_create_json_db_backend():
    with pytest.raises(TypeError):
        create_json_db_backend()
    with pytest.raises(FileNotFoundError):
        create_json_db_backend("")
    with pytest.raises(TypeError):
        create_json_db_backend(db_file_path, db_file_path)


# Test get db client
def test_get_db_client(remove_file):
    # Remove db json file before creating a new one with the same name
    remove_file(db_file_path)

    create_json_db_backend(db_file_path)
    assert os.path.isfile(db_file_path)

    client: Client = get_db_client(db_file_path)
    assert isinstance(client, Client)


# Test wrong get db client
def test_wrong_get_db_client(remove_file):
    # Remove db json file before creating a new one with the same name
    remove_file(db_file_path)

    create_json_db_backend(db_file_path)
    assert os.path.isfile(db_file_path)

    with pytest.raises(TypeError):
        _: Client = get_db_client()
    with pytest.raises(TypeError):
        _: Client = get_db_client(db_file_path, db_file_path)


# Test get json db backend
def test_get_json_db_backend(remove_file):
    # Remove db json file before creating a new one with the same name
    remove_file(db_file_path)

    create_json_db_backend(db_file_path)
    assert os.path.isfile(db_file_path)

    db: JSONBackend = get_json_db_backend(db_file_path)
    assert isinstance(db, JSONBackend)


# Test doubled get json db backend
def test_doubled_get_json_db_backend(remove_file):
    # Remove db json file before creating a new one with the same name
    remove_file(db_file_path)

    create_json_db_backend(db_file_path)
    assert os.path.isfile(db_file_path)

    db: JSONBackend = get_json_db_backend(db_file_path)
    assert isinstance(db, JSONBackend)

    db: JSONBackend = get_json_db_backend(db_file_path)
    assert isinstance(db, JSONBackend)


# Test wrong get json db backend
def test_wrong_get_json_db_backend(remove_file):
    # Remove db json file before creating a new one with the same name
    remove_file(db_file_path)

    create_json_db_backend(db_file_path)
    assert os.path.isfile(db_file_path)

    with pytest.raises(TypeError):
        _: JSONBackend = get_json_db_backend()
    with pytest.raises(TypeError):
        _: JSONBackend = get_json_db_backend(db_file_path, db_file_path)


# Test get db client from backend
def test_get_db_client_from_backend(remove_file):
    # Remove db json file before creating a new one with the same name
    remove_file(db_file_path)

    create_json_db_backend(db_file_path)
    assert os.path.isfile(db_file_path)

    db: JSONBackend = get_json_db_backend(db_file_path)
    assert isinstance(db, JSONBackend)

    client: Client = get_db_client_from_backend(db)
    assert isinstance(client, Client)


# Test wrong get db client from backend
def test_wrong_get_db_client_from_backend(remove_file):
    # Remove db json file before creating a new one with the same name
    remove_file(db_file_path)

    create_json_db_backend(db_file_path)
    assert os.path.isfile(db_file_path)

    db: JSONBackend = get_json_db_backend(db_file_path)
    assert isinstance(db, JSONBackend)

    with pytest.raises(TypeError):
        _: Client = get_db_client_from_backend()
    with pytest.raises(TypeError):
        _: Client = get_db_client_from_backend(db, db)


# Define class for following tests:
#   - test_get_container_from_db
#   - test_get_device_from_container()
class _TestContainer(HappiItem):
    model_no = EntryInfo(
        "Model number",
        optional=False,
        enforce=int,
        default=0,
        enforce_doc="This must be a number",
    )


# Test get container from db
def test_get_container_from_db(remove_file):
    # Remove db json file before creating a new one with the same name
    remove_file(db_file_path)

    create_json_db_backend(db_file_path)
    assert os.path.isfile(db_file_path)

    client: Client = get_db_client(db_file_path)
    assert isinstance(client, Client)

    # Create container
    container_name: str = "v3"
    container: _TestContainer = _TestContainer(name=container_name)
    container.model_no = 12345

    # Add container to registry
    add_to_registry("_TestContainer", _TestContainer)

    # Add container to database
    client.add_item(container)

    # Retrieve container from database
    container: HappiItem | None = get_container_from_db(client, container_name)
    assert isinstance(container, HappiItem)
    assert isinstance(container, _TestContainer)
    assert container.name == container_name


# Test get not existing container from db
def test_get_not_existing_container_from_db(remove_file, capsys):
    # Remove db json file before creating a new one with the same name
    remove_file(db_file_path)

    create_json_db_backend(db_file_path)
    assert os.path.isfile(db_file_path)

    client: Client = get_db_client(db_file_path)
    assert isinstance(client, Client)

    # Create container
    container_name: str = "v3"
    container: _TestContainer = _TestContainer(name=container_name)
    container.model_no = 12345

    # Add container to registry
    add_to_registry("_TestContainer", _TestContainer)

    # Add container to database
    client.add_item(container)

    # Retrieve not existing container from database
    container_name: str = "WrongName"
    container: HappiItem | None = get_container_from_db(client, container_name)
    assert container is None

    # Capture message generated by "get_container_from_db".
    captured = capsys.readouterr()
    assert (
        "Get container with name WrongName from db FAILED. Exception msg: No item information found that matches the search criteria"
        in captured.out
    )


# Define class for following test:
#   - test_get_device_from_container()
class _TestDevice:
    def __init__(self, prefix: str, name: str, labels: list, read_attrs: list):
        self.prefix = prefix
        self.name = name
        self.labels = set(labels)
        self.read_attrs = read_attrs


# Test get device from container
def test_get_device_from_container(remove_file):
    # Remove db json file before creating a new one with the same name
    remove_file(db_file_path)

    create_json_db_backend(db_file_path)
    assert os.path.isfile(db_file_path)

    client: Client = get_db_client(db_file_path)
    assert isinstance(client, Client)

    # Create container
    container_name: str = "v3"
    prefix_name: str = "PV:M1"
    labels: list = ["detectors"]
    read_attrs: list = ["readback", "clear"]

    _kwargs: dict = {}
    _kwargs["prefix"] = prefix_name
    _kwargs["name"] = container_name
    _kwargs["labels"] = labels
    _kwargs["read_attrs"] = read_attrs

    container: _TestContainer = _TestContainer(
        name=container_name,
        device_class="test_happi_utils._TestDevice",
        kwargs=_kwargs,
    )
    container.model_no = 12345

    # Add container to registry
    add_to_registry("_TestContainer", _TestContainer)

    # Add container to database
    client.add_item(container)
    # Retrieve container from database
    container: HappiItem = get_container_from_db(client, container_name)

    assert isinstance(container, HappiItem)
    assert isinstance(container, _TestContainer)
    assert container.name == container_name

    extracted_obj = get_device_from_container(container)
    assert extracted_obj.prefix == prefix_name
    assert extracted_obj.name == container_name
    assert extracted_obj.labels == set(labels)
    assert extracted_obj.read_attrs == read_attrs

    assert extracted_obj.md.model_no == 12345


# Remove db file at the end of the test
def test_remove_db_file(remove_file):
    remove_file(db_file_path)
