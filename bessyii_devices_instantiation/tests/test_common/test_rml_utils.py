"""Module:              test_rml_utils.py

Description:            Test functionalities of Rml class from rml_utils.py

Actions:
                        Test if data dir exists
                        Test if test rml-file exists
                        Test instantiation of Rml class
                        Test not possible instantiation of Rml class
                        Test methods:
                            - get_device_type()
                            - get_value()
                            - get_attributes()

Test data directory:    .data
Test file:              .data/optics_trial.rml
"""

import os
import pytest

from bessyii_devices_instantiation.common.rml_utils import Rml

# Define path to data folder
data_folder: str = os.path.join(os.path.dirname(__file__), "data")

# Define path to rml-file serving as "test data"
file_name: str = "optics_trial.rml"
file_path: str = os.path.join(data_folder, file_name)


# Test if "data_folder" is pointing to a folder
def test_data_dir():
    assert os.path.isdir(data_folder)


# Test if "file_path" is pointing to a file
def test_data_file():
    assert os.path.isfile(file_path)


# Test instantiation of Rml
def test_instantiation():
    rml: Rml = Rml(file_path)
    assert isinstance(rml, Rml)


# Test not instantiation of Rml
def test_not_instantiation():
    with pytest.raises(TypeError):
        _: Rml = Rml()
    with pytest.raises(ValueError):
        _: Rml = Rml("")
    with pytest.raises(TypeError):
        _: Rml = Rml(file_path, file_path)


# Fixture for test of methods
@pytest.fixture(scope="module")
def rml_instance():
    rml: Rml = Rml(file_path)
    yield rml


# Test of get device type for a given correct device_name
@pytest.mark.parametrize(
    ("device_name, expected_device_type"),
    (
        ("CircleSource", "CircleSource"),
        ("m13", "Toroid"),
        ("v4", "PlaneMirror"),
    ),
)
def test_get_device_type(
    rml_instance: Rml, device_name: str, expected_device_type: str
):
    actual: str | None = rml_instance.get_device_type(device_name)
    assert actual == expected_device_type


# Test of get device type for a given WRONG device_name.
def test_get_device_type_for_wrong_device_name(rml_instance: Rml):
    device_name: str = "wrongDevice"
    actual: str | None = rml_instance.get_device_type(device_name)
    assert actual is None


# Test of get value for given correct device_name and correct child_elements
@pytest.mark.parametrize(
    ("device_name, child_elements, expected_value"),
    (
        (
            "CircleSource",
            ["numberRays"],
            "20000",
        ),
        ("m13", ["materialCoating1"], "ab"),
        (
            "m13",
            ["thicknessCoating1"],
            "1.567",
        ),
        ("m13", ["worldPosition", "x"], "1.2000000000000001"),
        ("m13", ["worldPosition", "y"], "3.4000000000000002"),
        ("m13", ["worldPosition", "z"], "5.6000000000000003"),
        ("v4", ["materialCoating1"], "cd"),
        (
            "v4",
            ["thicknessCoating1"],
            "2.987",
        ),
        ("v4", ["worldPosition", "x"], "7.8000000000000004"),
        ("v4", ["worldPosition", "y"], "8.9000000000000005"),
        ("v4", ["worldPosition", "z"], "10.1000000000000006"),
    ),
)
def test_get_value(
    rml_instance: Rml, device_name: str, child_elements: list[str], expected_value: str
):
    actual: str = rml_instance.get_value(device_name, child_elements)
    assert actual == expected_value


# Test of get value for given WRONG device_name and correct child_elements
def test_get_value_for_wrong_device_name(rml_instance: Rml):
    device_name: str = "wrongDevice"
    child_elements: list[str] = ["thicknessCoating1"]
    with pytest.raises(AttributeError):
        _: str = rml_instance.get_value(device_name, child_elements)


# Test of get value for given correct device_name and WRONG child_elements
def test_get_value_for_wrong_child_elements(rml_instance: Rml):
    device_name: str = "v4"
    child_elements: list[str] = ["wrongElement"]
    with pytest.raises(AttributeError):
        _: str = rml_instance.get_value(device_name, child_elements)


# Test of get attributes for given correct device_name and correct child_elements
@pytest.mark.parametrize(
    ("device_name, child_elements, expected_attributes"),
    (
        (
            "CircleSource",
            ["numberRays"],
            {"id": "numberRays", "enabled": "T"},
        ),
        ("m13", ["materialCoating1"], {"id": "materialCoating1", "enabled": "F"}),
        (
            "m13",
            ["thicknessCoating1"],
            {"id": "thicknessCoating1", "enabled": "T"},
        ),
        ("m13", ["worldPosition", "x"], {}),
        ("m13", ["worldPosition", "y"], {}),
        ("m13", ["worldPosition", "z"], {}),
        ("v4", ["materialCoating1"], {"id": "materialCoating1", "enabled": "T"}),
        (
            "v4",
            ["thicknessCoating1"],
            {"id": "thicknessCoating1", "enabled": "F"},
        ),
        ("v4", ["worldPosition", "x"], {}),
        ("v4", ["worldPosition", "y"], {}),
        ("v4", ["worldPosition", "z"], {}),
    ),
)
def test_get_attributes(
    rml_instance: Rml,
    device_name: str,
    child_elements: list[str],
    expected_attributes: dict,
):
    actual: dict = rml_instance.get_attributes(device_name, child_elements)
    assert actual == expected_attributes


# Test of get attributes for given WRONG device_name and correct child_elements
def test_get_attributes_for_wrong_device_name(rml_instance):
    device_name: str = "wrongDevice"
    child_elements: list[str] = ["thicknessCoating1"]
    with pytest.raises(AttributeError):
        _: dict = rml_instance.get_attributes(device_name, child_elements)


# Test of get attributes for given correct device_name and WRONG child_elements
def test_get_attributes_for_wrong_child_elements(rml_instance):
    device_name: str = "v4"
    child_elements: list[str] = ["wrongElement"]
    with pytest.raises(AttributeError):
        _: dict = rml_instance.get_attributes(device_name, child_elements)
