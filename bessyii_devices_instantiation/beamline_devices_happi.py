"""Module:      beamline_devices_happi.py

Description:    Support for instatiation of beamline devices by means of happi.
                This module creates dynamically custom happi containers for devices.

Usage:          Import whole module or function(s) from the module. Supported functions:
                    - create_containers()
                    - _create_container()

                    Debug only:
                    - print_container_content()
                    - print_device_metadata()
                    - print_device_mandatory_info()
"""

from happi.item import EntryInfo, HappiItem

from bessyii_devices_instantiation.beamline_devices_const import (
    MANDATORY_KEY_NAMES,
    DeviceRmlMetaData,
)
from bessyii_devices_instantiation.common.happi_utils import add_to_registry


class DevCon(HappiItem):
    """This is a custom happi container (DevCon =: device container) serving as a base container for all inheriting containers (childs).
    This class has to inherit from HappiItem class to be able to get entered into Happi database
    This class has attributes of type EntryInfo which are common to all childs:
        - type (str): Device type name
        - active (bool):Device active. Active device is present in database and can be instantiated. Inactive device is present in database but will not be instantiated
        - connection_timeout(float): Timeout value applied by the connection to ophyd or ophyd-async device.

    Parameters
    ----------
    HappiItem : HappiItem
        Base class
    """

    type = EntryInfo(
        "Device type name",
        optional=False,
        enforce=str,
        enforce_doc="This must be a string",
    )
    active = EntryInfo(
        "Device active. Active device is present in database and can be instantiated. Inactive device is present in database but will not be instantiated",
        optional=False,
        enforce=bool,
        enforce_doc="This must be a boolean, True/False",
    )
    connection_timeout = EntryInfo(
        "Device connection timeout. Applied for ophyd and ophyd-async devices.",
        optional=False,
        enforce=float,
        enforce_doc="This must be a float",
    )


def create_containers(
    device_cfg_list: list[dict],
    rml_metadata: dict[str, DeviceRmlMetaData],
) -> list[DevCon]:
    """Create beamline device containers

    Parameters
    ----------
    device_cfg_list : list[dict]
        Device configurations from cfg y(aml) file
        List of key names, necessary for device instantiation. These keys are mandatory for each device in device configuration y(a)ml file
    rml_metadata : Dict[str, DeviceRmlMetaData]
        Metadata from rml file for device instances

    Returns
    -------
    list[DevCon]
        List of created device containers
    """

    # Collection of containers
    containers: list[DevCon] = []
    for idx, cfg in enumerate(device_cfg_list):
        rml_md: DeviceRmlMetaData = {}
        if cfg["instance_name"] in rml_metadata:
            rml_md = rml_metadata[cfg["instance_name"]]

        container: DevCon = _create_container(idx, cfg, rml_md)
        containers.append(container)
    return containers


def _create_container(idx: int, cfg: dict, rml_md: DeviceRmlMetaData) -> DevCon:
    """Create beamline device container for given device configuration and saved it in happi registry

    Parameters
    ----------EntryInfo(
                f"RML metadata for key: {key}",
                optional=False,
                enforce=dict,
                enforce_doc="This must be a dict",
    idx : int
        Each container has to be saved in registry under an unique name, index is necessary
    cfg : dict
        Device configurations from cfg y(a)ml file
    rml_md : DeviceRmlMetaData
        Metadata from rml file for given device instance. Empty dictionary if no metadata for the given device, because:
            Reason 1: apply_rml_on=False (dev config y(a)ml file)
            Reason 2: There is no metadata for given device instance in rml-file

    Returns
    -------
    DevCon
        Device container

    Raises
    ------
    ValueError
        Some value in rml_md:DeviceRmlMetaData is not of type dict or str
    """

    # Define all attributes for the container class. Do not define attributes for "MANDATORY_KEY_NAMES"
    attributes: dict[EntryInfo] = {}

    # Attributes (metadata) from device config file
    for key in cfg.keys():
        if key not in MANDATORY_KEY_NAMES:
            attributes[key] = EntryInfo(
                f"Metadata of key: {key}",
                optional=False,
                enforce=str,
                enforce_doc="This must be a string",
            )

    # Attributes (metadata) from rml file
    for key, value in rml_md.items():
        if isinstance(value, dict):
            attributes[key] = EntryInfo(
                f"RML metadata for key: {key}",
                optional=False,
                enforce=dict,
                enforce_doc="This must be a dict",
            )
        elif isinstance(value, str):
            attributes[key] = EntryInfo(
                f"RML metadata for key: {key}",
                optional=False,
                enforce=str,
                enforce_doc="This must be a string",
            )
        else:
            raise ValueError("Value: {value} is not of type dict or str")

    def get_attributes(self):
        """
        Retrieve all metadata attributes of the instance. I.e. all metadata managed in "attributes"
        and also ["type", "active", "connection_timeout"] managed in base class
        This function can be used like this: instantiated_object.md.get_attributes()

        Returns
        -------
        md_attributes: dict
            all metadata attributes with their corresponding values.
            E.g. {"worldPosition" : {'x':1,'y':2,'z':3}}
        """

        md_attributes: dict = {}
        for key, value in self.__dict__.items():
            if (key in attributes.keys()) or (
                key in ["type", "active", "connection_timeout"]
            ):
                md_attributes[key] = value
        return md_attributes

    methods: dict = {"get_attributes": get_attributes}

    # Create dynamically device container class
    DeviceContainer = type(
        "DeviceContainer" + cfg["type"], (DevCon,), {**methods, **attributes}
    )

    # Prepare kwargs, necessary for creation of device container
    _kwargs: dict = {}

    # "prefix" and "name" are always necessary be passed to class that is being instantiated
    _kwargs["prefix"] = cfg["prefix"]
    _kwargs["name"] = cfg["instance_name"]

    # Add additional kwargs, which were found in the device configuration y(a)ml file
    # Three possibilities for class_kwargs list read from device configuration file could be:
    # - A valid not empty list:
    #       cfg.get("class_kwargs") = [{'read_attrs': ['readback', 'reset']}, {'labels': ['detectors']}, {'s_labels': 's_label_value'}]
    # - A valid empty ist:
    #       cfg.get("class_kwargs") = []
    # - None:
    #       cfg.get("class_kwargs") = None
    #
    for _dict in cfg.get("class_kwargs") or []:
        _kwargs.update(_dict)
    
    # Add args, which were found in the device configuration y(a)ml file
    # Three possibilities for class_args list read from device configuration file could be:
    # - A valid not empty list:
    #       cfg.get("class_args") = ['arg1', 'arg2']
    # - A valid empty ist:
    #       cfg.get("class_args") = []
    # - None:
    #       cfg.get("class_args") = None
    #
    _args = list(cfg.get("class_args") or [])
    
    # Create device container from device container class
    container = DeviceContainer(
        name=cfg["instance_name"],
        device_class=cfg["class"],
        args = _args,
        kwargs=_kwargs,
    )

    # Assignment of mandatory properties/attributes which are defined in base class
    container.type = cfg["type"]
    container.active = cfg["active"]
    container.connection_timeout = cfg["connection_timeout"]

    # Assignment of properties/attributes defined in device container:

    #   -> for arbitrary metadata from device config y(a)ml file
    for key in cfg.keys():
        if key not in MANDATORY_KEY_NAMES:
            setattr(container, key, cfg[key])

    #   -> for metadata from rml file
    for key in rml_md.keys():
        setattr(container, key, rml_md[key])

    # Add device container to happi registry
    add_to_registry("DeviceContainer" + str(idx), DeviceContainer)

    return container

# -------------------------------Debug only-------------------------------
def print_container_content(container: DevCon) -> None:
    """Print content of the container

    Parameters
    ----------
    container : DevCon
        Container of base type DevCon: Any child of DevCon
    """

    print("Content of container:")
    print(dict(container))


def print_device_metadata(device: object) -> None:
    """Print metadata "name: value" associated with a device

    Parameters
    ----------
    device : object
        Any device instance
    """

    print("*** Device metadata info ***")
    print("'name' : 'value'")

    names = device.md.info_names
    for name in names:
        value = getattr(device.md, name)
        print(f"{name} : {value}")

    print("*** End of Device metadata info ***")


def print_device_mandatory_info(device: object) -> None:
    """Print mandatory info a device has to be provided with

    Parameters
    ----------
    device : object
        Any device instance
    """

    print("*** Device mandatory info ***")
    [print(item) for item in device.md.mandatory_info]
    print("*** End of Device mandatory info ***")
# -------------------------------End of Debug only------------------------
