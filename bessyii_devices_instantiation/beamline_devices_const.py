"""Constants and type aliases
------
Constants:

    MANDATORY_KEY_NAMES
        Nine mandatory properties/key names for each device in device configuration y(a)ml-file:
                "type",                mandatory, not empty, not unique. Device type.  A good practice could be to use a 'type' of object(device) from rml file if given device is present there or alternatively name of the class the device belongs to, e.g. "PositionerBessyValve" or any other arbitrary value.
                "active"               mandatory, not empty, not unique. Device is active/enabled: {True, False}. Active/enabled device is present in database and can be instantiated. Inactive/disabled device is present in database but will not be instantiated.
                "class",               mandatory, not empty, not unique. Class name necessary to instantiate object, e.g. "bessyii_devices.valve.PositionerBessyValve".
                "prefix",              mandatory, not empty, unique.     Name of the PV, e.g. "V03V11B001L:".
                "instance_name",       mandatory, not empty, unique,     Instance name, e.g. "v3".
                "class_kwargs"         mandatory, not empty if class instantiation requires kwargs, empty if class does not require kwargs, in empty case please write only "class_kwargs:" or "class_kwargs: []", not unique.
                "class_args"           mandatory, not empty if class instantiation requires args, empty if class does not require args, in empty case please write only "class_args:" or "class_args: []", not unique.
                "connection_timeout",  mandatory, not empty, not unique. Timeout value used in ophyd function wait_for_connection(). Allowed input types: str, int or float.
                "apply_rml_on",        mandatory, not empty, not unique. Extraction of metadata from rml-file and its incorporation into happi container {True, False}.

    UNIQUE_VALUE_KEY_NAMES
        Key names for which the corresponding values should be unique in device configuration y(a)ml file
        Values of key names: "prefix" and "instance_name" have to be unique in  device configuration y(a)ml-file


Type aliases:

        DeviceRmlMetaData : dict[str, Union[dict, str]]
            dict[str="device_instance_name", Union[dict="metadata", str="metadata"]]
            Alias describing structure of device metadata extracted from rml-file (for a device)
"""

from typing import NewType, Union

# from typing import Union

# ------------------------------------------------- CONSTANTS -----------------------
MANDATORY_KEY_NAMES: list[str] = [
    "type",
    "active",
    "class",
    "prefix",
    "instance_name",
    "class_kwargs",
    "class_args",
    "connection_timeout",
    "apply_rml_on",
]
UNIQUE_VALUE_KEY_NAMES: list[str] = ["prefix", "instance_name"]
# ------------------------------------------------- End of CONSTANTS ----------------

# ------------------------------------------------- TYPE ALIASES -------------------------
DeviceRmlMetaData = NewType("DeviceRmlMetaData", dict[str, Union[dict, str]])
# type DeviceRmlMetaData = dict[str, Union[dict, str]]  # Since python 3.12
# ------------------------------------------------- End of TYPE ALIASES ------------------
