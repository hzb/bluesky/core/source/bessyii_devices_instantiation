"""Definitions

DIR PATHS
------
    Matched to a directory tree structure of the machine (docker container)
    letting run this application:

        _DEV_CFG_FILE_DIR_PATH : str
        _RML_FILE_DIR_PATH : str
        _HAPPI_DB_FILE_DIR_PATH : str
        _DEV_INSTANTIATION_SETUP_FILE_DIR_PATH : str


FILE NAMES
------
    File names read from device configuration setup yaml file:
        DEV_CFG_FILE_NAME : str
        RML_FILE_NAME : str
        HAPPI_DB_FILE_NAME : str
"""

import os

from bessyii_devices_instantiation.common.yaml_utils import read_yaml

# ------------------------------------------------- DIR PATHS  --------------------------------
# Extract from env variable dev cfg file dir path (in container)
_DEV_CFG_FILE_DIR_PATH = os.environ.get("_DEV_CFG_FILE_DIR_PATH")
if _DEV_CFG_FILE_DIR_PATH is None:
    raise ValueError(
        "_DEV_CFG_FILE_DIR_PATH is None. Check content of ~/.bluesky_config file."
    )

# Extract from env variable the rml file dir path (in container)
_RML_FILE_DIR_PATH = os.environ.get("_RML_FILE_DIR_PATH")
if _RML_FILE_DIR_PATH is None:
    raise ValueError(
        "_RML_FILE_DIR_PATH is None. Check content of ~/.bluesky_config file."
    )

# Extract from env variable the happi db file dir path (in container)
_HAPPI_DB_FILE_DIR_PATH = os.environ.get("_HAPPI_DB_FILE_DIR_PATH")
if _HAPPI_DB_FILE_DIR_PATH is None:
    raise ValueError(
        "_HAPPI_DB_FILE_DIR_PATH is None. Check content of ~/.bluesky_config file."
    )

# Extract from env variable the dev instantiation setup file dir path (in container)
_DEV_INSTANTIATION_SETUP_FILE_DIR_PATH = os.environ.get(
    "_DEV_INSTANTIATION_SETUP_FILE_DIR_PATH"
)
if _DEV_INSTANTIATION_SETUP_FILE_DIR_PATH is None:
    raise ValueError(
        "_DEV_INSTANTIATION_SETUP_FILE_DIR_PATH is None. Check content of ~/.bluesky_config file."
    )
# ------------------------------------------------- End of DIR PATHS  ---------------------

# ------------------------------------------------- FILE NAMES  ---------------------------

# Extract from env variable the name of device instantiation setup y(a)ml file
_DEV_INSTANTIATION_SETUP_FILE_NAME = os.environ.get("DEV_INSTANTIATION_SETUP_FILE_NAME")
if _DEV_INSTANTIATION_SETUP_FILE_NAME is None:
    raise ValueError(
        "DEV_INSTANTIATION_SETUP_FILE_NAME is None. Check content of ~/.bluesky_config file."
    )

# ------------------------------------------------- End of FILE NAMES  --------------------


dev_instantiation_setup_file_path = os.path.join(
    _DEV_INSTANTIATION_SETUP_FILE_DIR_PATH, _DEV_INSTANTIATION_SETUP_FILE_NAME
)

# Read from device instantiation setup y(a)ml file values (file names) for following keys:
#   - DEV_CFG_FILE_NAME
#   - RML_FILE_NAME
#   - HAPPI_DB_FILE_NAME

dev_instantiation_setup: dict = read_yaml(dev_instantiation_setup_file_path)
if not dev_instantiation_setup:
    raise ValueError(
        f"Device instantiation setup file cannot be empty. Check device instantiation setup file: {dev_instantiation_setup_file_path}"
    )

# Assign file names read from device configuration setup y(a)ml file
_DEV_CFG_FILE_NAME = dev_instantiation_setup["DEV_CFG_FILE_NAME"]
_RML_FILE_NAME = dev_instantiation_setup["RML_FILE_NAME"]
_HAPPI_DB_FILE_NAME = dev_instantiation_setup["HAPPI_DB_FILE_NAME"]

# Create paths to files
DEV_CFG_FILE_PATH = os.path.join(_DEV_CFG_FILE_DIR_PATH, _DEV_CFG_FILE_NAME)
RML_FILE_PATH = os.path.join(_RML_FILE_DIR_PATH, _RML_FILE_NAME)
HAPPI_DB_FILE_PATH = os.path.join(_HAPPI_DB_FILE_DIR_PATH, _HAPPI_DB_FILE_NAME)
