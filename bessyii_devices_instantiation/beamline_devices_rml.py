"""Module:      beamline_devices_rml.py

Description:    Support for extracting metadata information from rml file for different device types

Usage:          Import whole module or function(s) from the module. Supported functions:
                    - get_md_from_rml()
                    - _fetch_from_rml()
"""

from dataclasses import dataclass
from enum import Enum
from typing import Union

from bessyii_devices_instantiation.beamline_devices_const import DeviceRmlMetaData
from bessyii_devices_instantiation.common.rml_utils import Rml


class _OpticalElements(Enum):
    """Collection of optical elements applied at beamline.

    Enum Members
    ----------
    Value of enum must correspond exactly to value of "type" for any device used in rml-file.
    """

    SLIT = "Slit"
    APERTURE = "Aperture"
    FOIL = "Foil"
    ZONEPLATE = "Zoneplate"
    PLANEMIRROR = "PlaneMirror"
    CYLINDER = "Cylinder"
    CONE = "Cone"
    SPHERE = "Sphere"
    TOROID = "Toroid"
    ELLIPSOID = "Ellipsoid"
    PARABOLOID = "Paraboloid"
    EXPERTSOPTICS = "ExpertsOptics"
    ELLIPTICALTOROID = "EllipticalToroid"
    HYPERBOLOID = "Hyperboloid"
    PLANEGRATING = "PlaneGrating"
    SPHERICALGRATING = "SphericalGrating"
    TOROIDALGRATING = "ToroidalGrating"
    REFLECTIONZONEPLANE = "ReflectionZoneplate"
    CRYSTAL = "Crystal"
    CYLINDRICALCRYSTAL = "CylindricalCrystal"
    MONOCAPILLARY = "Monocapillary"


@dataclass
class _Point3D:
    """Represents a point in three-dimensional Cartesian coordinate system.
    Used in this context for serialization of "worldPosition"
    x (float, str): The x-coordinate of the point.
    y (float, str): The y-coordinate of the point.
    z (float, str): The z-coordinate of the point.
    """

    x: float | str
    y: float | str
    z: float | str

    __slots__ = ("x", "y", "z")

    def __json__(self) -> dict[str, Union[float, str]]:
        """Serialize object to a JSON-compatible dictionary

        Returns
        -------
        dict[str, Union[float, str]]
            JSON-compatible dictionary representation of the object.
        """
        return {
            "x": self.x,
            "y": self.y,
            "z": self.z,
        }


def get_md_from_rml(
    rml_file_path: str, dev_cfg_list: list[dict]
) -> dict[str, DeviceRmlMetaData]:
    """Get metadata from rml-file for devices listed in dev_cfg_list, for which: apply_rml_on=True

    Parameters
    ----------
    rml_file_path : str
        Path to rml-file
    dev_cfg_list : list[dict]
        Device configuration from cfg y(aml) file

    Returns
    -------
        dict[str, DeviceRmlMetaData] <-> dict[str, dict[str, Union[dict, str]]]
        Metadata for all device instances listed in dev_cfg_list for which: apply_rml_on=True
        under condition that device instance name can be found in rml file.
        Content of the metadata depends on device type which is fetched by function "fetch_from_rml" from rml file.
        "str" represent device instance name
    """
    # Result to be returned
    result: dict[str, DeviceRmlMetaData] = {}

    # Collect value of flag "apply_rml_on" for all devices listed in dev_cfg_list
    apply_rml_on: list[bool] = []
    for dev in dev_cfg_list:
        value: bool = dev["apply_rml_on"]
        apply_rml_on.append(value)

    # If no device is applying rml return result
    if all(not x for x in apply_rml_on):
        return result

    rml = Rml(rml_file_path)

    # Create an enum lookup to be able to get an enum: _OpticalElements for a given device type: str
    enum_lookup = {member.value: member for member in _OpticalElements}

    for dev in dev_cfg_list:
        instance_name: str = dev["instance_name"]
        apply_rml_on: bool = dev["apply_rml_on"]
        device_type: Union[str, None] = rml.get_device_type(instance_name)

        if (not apply_rml_on) or (device_type is None):
            continue
        else:
            type: _OpticalElements = enum_lookup[device_type]
            result[instance_name] = _fetch_from_rml(rml, instance_name, type)
    return result


def _fetch_from_rml(rml: Rml, name: str, type: _OpticalElements) -> DeviceRmlMetaData:
    """Fetch particular metadata from rml-file for a device ("name") depending on type ("type")

    Parameters
    ----------
    rml : Rml
        Rml object
    name : str
        Device instance name
    type : _OpticalElements
        Device type (enum)

    Returns
    -------
    DeviceRmlMetaData <-> Dict[str, Union[dict, str]]
        Metadata for given device instance name and device type

    Raises
    ------
    ValueError
        If no matching found for the type enum
    """

    result: DeviceRmlMetaData = {}

    # Collect particular metadata from rml depending on device type
    match type:
        case (
            _OpticalElements.TOROID
            | _OpticalElements.PLANEMIRROR
            | _OpticalElements.CYLINDER
            | _OpticalElements.ELLIPSOID
        ):
            child_elements = ["materialCoating1"]
            value = rml.get_value(name, child_elements)
            result[child_elements[0]] = value

            child_elements = ["thicknessCoating1"]
            value = rml.get_value(name, child_elements)
            result[child_elements[0]] = value

            child_elements = ["worldPosition", "x"]
            x = rml.get_value(name, child_elements)
            child_elements = ["worldPosition", "y"]
            y = rml.get_value(name, child_elements)
            child_elements = ["worldPosition", "z"]
            z = rml.get_value(name, child_elements)
            p = _Point3D(x, y, z)
            result[child_elements[0]] = p.__json__()

        case _OpticalElements.SLIT:
            child_elements = ["worldPosition", "x"]
            x = rml.get_value(name, child_elements)
            child_elements = ["worldPosition", "y"]
            y = rml.get_value(name, child_elements)
            child_elements = ["worldPosition", "z"]
            z = rml.get_value(name, child_elements)
            p = _Point3D(x, y, z)
            result[child_elements[0]] = p.__json__()

        case _:
            raise ValueError(
                "No case for _OpticalElements enum found. This is programming error."
            )
    return result
