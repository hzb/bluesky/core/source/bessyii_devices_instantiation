"""Module:      happi_utils.py

Description:    Support for using happi library

Usage:          Import whole module or function(s) from the module. Supported functions:
                    - add_to_registry()
                    - create_json_db_backend()
                    - get_db_client()
                    - get_json_db_backend()
                    - get_db_client_from_backend
                    - get_container_from_db()
                    - get_device_from_container()

                    Debug only:
                    - print_database_items()

Requirements:   happi==2.5.0
                pymongo==3.12.0 (only if MongoBackend should be used in this module, which is not a case now)
"""

from happi.client import Client
from happi.errors import SearchError
from happi.item import HappiItem
from happi.loader import from_container
from happi.backends.json_db import JSONBackend
from happi.containers import registry

def add_to_registry(item_name: str, container_class: HappiItem) -> None:
    """Add container to happi registry.

    Parameters
    ----------
    item_name : str
        Item name, e.g. "DeviceContainer7"
    container_class : HappiItem
        Container which inherits from HappiItem, e.g. DeviceContainer

    Returns
    -------
    None
    """
    registry[item_name] = container_class


def create_json_db_backend(db_file_path: str) -> None:
    """Create JSON db backend for storing containers based on device configuration parameters.
        This function should be called only once !

    Parameters
    ----------
    db_file_path : str
        Path to JSON db file to be created (e.g. ".../dev_cfg_db.json")

    Returns
    -------
    None
    """
    JSONBackend(path=db_file_path, initialize=True)


def get_db_client(db_file_path: str) -> Client:
    """Get db client to control the contents of the Happi Database

    Parameters
    ----------
    db_file_path : str
        Path to JSON file to be controlled (e.g. ".../dev_cfg_db.json")

    Returns
    -------
    Client
        Happi db client
    """
    return Client(path=db_file_path)


def get_json_db_backend(db_file_path: str) -> JSONBackend:
    """Get JSON db backend for storing containers based on device configuration parameters.

    Parameters
    ----------
    db_file_path : str
        Path to existing JSON file (e.g. ".../dev_cfg_db.json")

    Returns
    -------
    JSONBackend
        happi.backends.json_db.JSONBackend
    """
    return JSONBackend(path=db_file_path, initialize=False)


def get_db_client_from_backend(backend: JSONBackend) -> Client:
    """Get db client to control the contents of the Happi Database.

    Parameters
    ----------
    backend : JSONBackend
        JSONBackend object

    Returns
    -------
    Client
        Happi db client
    """
    return Client(database=backend)


def get_container_from_db(client: Client, container_name: str) -> HappiItem | None:
    """Find container in database with the name "container_name" and return it

    Parameters
    ----------
    client : Client
        Client to control the contents of the Happi Database.
    container_name : str
        Name of the container

    Returns
    -------
    HappiItem
        HappiItem container corresponding to container_name found
    None
        HappiItem container corresponding to container_name not found
    """

    try:
        container = client.find_item(name=container_name)
    except SearchError as error:
        print(
            f"Get container with name {container_name} from db FAILED. Exception msg: {error}"
        )
    else:
        return container


def get_device_from_container(container: HappiItem) -> object:
    """Create/instantiate a device from a given HappiItem container and return it.
        Attach all metadata to the created/instantiated device.

    Parameters
    ----------
    container : HappiItem
        HappiItem container as a "base" for device instantiation process

    Returns
    -------
    object
        Device object created/instantiated from given HappiItem container
    """

    # Create device from container; Intentionally we do NOT catch the "ophyd.signal.ConnectionTimeoutError" exception. Thus, if exception raised -> crash of application.
    device = from_container(container, attach_md=True)
    return device


# ----------------------Debug only----------------------
def print_database_items(database: JSONBackend) -> None:
    """Print all items from database

    Parameters
    ----------
    database : JSONBackend
        (happi.backends.json_db.JSONBackend): database backend
    """

    print("All database items:")
    print(database.all_items)


# ----------------------End of Debug only---------------
