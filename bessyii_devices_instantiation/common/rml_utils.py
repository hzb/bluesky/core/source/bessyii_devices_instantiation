""" Module:         rml_utils.py

    Description:    Support of reading information from rml-file.
                        class Rml
                            - get_device_type()
                            - get_value()
                            - get_attributes()

    Usage:          Import whole module, instantiate Rml class with a valid path to rml file.
"""

from raypyng.rml import BeamlineElement, RMLFile


class Rml:
    """Class supporting extraction of information from a rml file."""

    def __init__(self, file_path: str):
        self.beamline: BeamlineElement = RMLFile(file_path).beamline

    def get_device_type(self, device_name: str) -> str | None:
        """Get device type for a given device name. If device name not found returns None

        Parameters
        ----------
        device_name : str
            Device instance name, like applied in ophyd, e.g. "M1"

        Returns
        -------
        str | None
            Device type, e.g. "Toroid" or None if device_name not found
        """

        attributes: list[dict] = [
            child.attributes() for child in self.beamline.children()
        ]  # list[dict]: Dict describes attributes of a device in following way: {'name': "device_instance_name", 'type': "device_type"}
        for item in attributes:
            if device_name == item["name"]:
                return item["type"]
        return None

    def get_value(self, device_name: str, child_elements: list[str]) -> str:
        """For given device name get the value of the last element of the list "child_elements".
        Do NOT include in the list "child_elements" name of the first child element ("beamline")
        present in each rml-file.

        Parameters
        ----------
        device_name : str
            Device instance name, e.g. "M1", or "KB1"
        child_elements : list[str]
            List of child elements' names forming a 'path' (from top to bottom)
            given by structure of the rml-file, e.g. ["worldPosition", "x"] or ["worldPosition", "y"] or ["materialCoating1"]

        Returns
        -------
        str
            Value of the last element in 'child_elements'
        """
        obj = getattr(self.beamline, device_name)
        for item in child_elements:
            obj = getattr(obj, item)
        return obj.cdata

    def get_attributes(self, device_name: str, child_elements: list[str]) -> dict:
        """For given device name get the attributes of the last element of the  list "child_elements".
        Do NOT include in the list "child_elements" name of the first child element ("beamline")
        present in each rml-file.

        Parameters
        ----------
        device_name : str
            Device instance name, e.g. "M1", or "KB1"
        child_elements : list[str]
            List of child elements' names forming a 'path' (from top to bottom)
            given by structure of the rml-file, e.g. ["worldPosition", "x"] or ["worldPosition", "y"] or ["materialCoating1"]

        Returns
        -------
        dict
            Attributes of the last element in 'child_elements'
        """
        obj = getattr(self.beamline, device_name)
        for item in child_elements:
            obj = getattr(obj, item)
        return obj.attributes()
