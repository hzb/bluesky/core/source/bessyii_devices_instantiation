"""Module:      beamline_devices.py

Description:    Creation of json database file based on beamline specific device cfg y(a)ml file.
                Incorporation of arbitrary metadata from device cfg y(a)ml file into happi containers.
                Incorporation of particular metadata (depending on device type) from rml-file into happi containers.
                Instantiation of beamline devices based on happi containers stored in json database file.

Usage:          Import whole module or function(s) from the module. Supported functions:
                    - instantiate()
                    - wait_for_device_connection()
                    - create_global_variables()


"""

import os
import asyncio

from bluesky.run_engine import RunEngine
from colorama import Fore, Style

from ophyd_async.core import Device as OphydAsyncDevice
from ophyd_async.core import NotConnected as OphydAsyncNC
from ophyd.device import Device as OphydDevice
from ophyd_async.plan_stubs import ensure_connected
from typing import Optional

from bessyii_devices_instantiation.beamline_devices_cfg import verify_dev_cfg
from bessyii_devices_instantiation.beamline_devices_const import DeviceRmlMetaData
from bessyii_devices_instantiation.beamline_devices_def import (
    DEV_CFG_FILE_PATH,
    RML_FILE_PATH,
    HAPPI_DB_FILE_PATH,
)
from bessyii_devices_instantiation.beamline_devices_happi import create_containers
from bessyii_devices_instantiation.beamline_devices_rml import get_md_from_rml
from bessyii_devices_instantiation.common.yaml_utils import read_yaml
from bessyii_devices_instantiation.common.happi_utils import (
    create_json_db_backend,
    get_container_from_db,
    get_db_client,
    get_device_from_container,
    Client,
    HappiItem,
)


# Instantiate devices
def instantiate(
    dev_cfg_file_path: str = DEV_CFG_FILE_PATH,
    db_file_path: str = HAPPI_DB_FILE_PATH,
    rml_file_path: str = RML_FILE_PATH,
) -> dict[str, object]:
    """
    Instantiate devices based on device configuration, RML, and Happi database files.

    This function handles the following steps:
    - Reads and processes the device configuration file.
    - Verifies the configuration.
    - Prepares the Happi database and applies any metadata (RML).
    - Instantiates devices based on the data retrieved from the Happi database.

    Parameters
    ----------
    dev_cfg_file_path : str, optional
        Path to the device configuration file (in YAML format). Defaults to `DEV_CFG_FILE_PATH`.
    
    db_file_path : str, optional
        Path to the Happi database file. Defaults to `HAPPI_DB_FILE_PATH`.

    rml_file_path : str, optional
        Path to the RML (metadata) file. Defaults to `RML_FILE_PATH`. If an empty string is provided,
        this indicates that no metadata should be applied (i.e., the "apply_rml_on" flag in the config file 
        is set to False for all devices).

    Returns
    -------
    dict[str, object]
        A dictionary where the keys are device instance names (as defined in the configuration file) and
        the values are the instantiated device objects. If no devices are instantiated, an empty dictionary is returned.

    Raises
    ------
    ValueError
        If a container for a device is found to be `None` or if the device container is flagged as inactive.

    Notes
    -----
    - The function reads the configuration file to determine the devices to instantiate.
    - Devices are only instantiated if the corresponding container is active.
    - The `get_db_client` function is used to interact with the Happi database.
    - The resulting device instances are returned in a dictionary, indexed by their instance names.
    """

    print(Fore.BLUE + "Beamline device instantiation started." + Style.RESET_ALL)

    # Read device configuration file
    dev_cfg_list: list[dict] | dict | None = read_yaml(dev_cfg_file_path)

    # Verify device configuration
    verify_dev_cfg(dev_cfg_list)

    # Prepare database
    _prepare_database(dev_cfg_list, db_file_path, rml_file_path)

    # Prepare "dev_instance_names". This list has to be unique which is already asserted by a check in verify_dev_cfg()
    dev_instance_names: list[str] = [item["instance_name"] for item in dev_cfg_list]

    # Get db client
    client: Client = get_db_client(db_file_path)

    devices: dict = {}

    for name in dev_instance_names:
        container: HappiItem | None = get_container_from_db(client, name)
        if container is None:
            raise ValueError(
                f"Instantiate devices FAILED. Container value for instance name: {name} has value None."
            )

        # Instantiate device only if the container is flagged as active. This flag is comming from device cfg file y(a)ml.
        # This flag is contained in each container of the happi database
        if container.active is True:
            devices[name] = get_device_from_container(container)

    print(
        f"Number of instantiated devices: {len(devices)} {Fore.RED + f'. Check device config file: {dev_cfg_file_path}' + Style.RESET_ALL if not devices else ''}"
    )

    print(Fore.BLUE + "Beamline device instantiation finished." + Style.RESET_ALL)
    return devices


def wait_for_device_connection(devices: dict[str, object], run_engine: Optional[RunEngine] = None) -> None:
    """
    Establishes connections to a collection of devices using the appropriate method.

    This function iterates through a dictionary of devices, determining whether each device
    requires synchronous or asynchronous connection handling. It then applies the correct
    connection method and prints the final connection status for each device.

    Parameters
    ----------
    devices : dict[str, object]
        A dictionary mapping device names to device objects. The devices can be of either synchronous
        (OphydDevice) or asynchronous (OphydAsyncDevice) types.
    run_engine : RunEngine, optional
        The Bluesky RunEngine instance used to execute connection plans for asynchronous devices.
        This parameter is required if there are any `OphydAsyncDevice` instances in the `devices` dictionary.
        If only synchronous devices are being connected, this parameter can be omitted.

    Returns
    -------
    None
        This function does not return a value but attempts to establish connections for all devices.

    Raises
    ------
    ValueError
        Raised if an asynchronous device (`OphydAsyncDevice`) is detected but no RunEngine (`run_engine`) is provided.
    NotConnected
        Raised if an asynchronous device fails to connect within the specified timeout.
    TimeoutError
        Raised if a synchronous device fails to connect within the specified timeout.

    Notes
    -----
    - `OphydDevice` instances are connected synchronously using `wait_for_connection()`.
    - `OphydAsyncDevice` instances are connected asynchronously using the `RunEngine`, if provided.
    - If a device type is unsupported, an error message is displayed.
    - After attempting all connections, the function prints the connection status for each device.
    """

    def connect_to_async_device(
        obj: object, timeout: float, mock: bool, run_engine: RunEngine
    ) -> None:
        """
        Connects an asynchronous device using the Bluesky RunEngine, with optional mocking.

        This function attempts to establish an asynchronous connection to the given device
        using the provided RunEngine instance. It allows specifying a timeout for the
        connection attempt and the option to mock the connection for testing purposes.

        Parameters
        ----------
        obj : object
            The asynchronous device object that supports a `connect()` method for establishing
            an asynchronous connection.
        timeout : float
            The maximum time (in seconds) to wait for the connection attempt to complete.
        mock : bool
            If True, the connection will be simulated (mocked). If False, a real connection
            attempt will be made. Default is False.
        run_engine : RunEngine
            The Bluesky RunEngine instance used to execute the connection plan. This is required
            to run asynchronous connection tasks for the device.

        Returns
        -------
        None
            This function does not return a value but executes the connection process using the
            RunEngine.

        Raises
        ------
        ValueError
            Raised if the `run_engine` argument is None.
        NotConnected
            Raised if the device fails to connect within the specified timeout.
        OphydAsyncNC
            Raised if the device's `connect()` method raises an error during the connection attempt.

        Notes
        -----
        - The `connected` attribute is manually set on the device object (`obj`) since `ophyd-async`
        does not provide it by default.
        - If the connection fails, an error message is printed with details about the failure,
        including the device name and exception message.
        - This function is intended for connecting asynchronous devices that require a `RunEngine`
        to manage their connection process.
        """

        if run_engine is None:
            raise ValueError(
                f"RunEngine instance is required to connect to asynchronous device: {obj.name}"
            )

        try:
            run_engine(ensure_connected(obj, timeout=timeout, mock=mock))
            obj.connected = True  # ophyd-async does not provide the attribute 'connected' for its device. That is why added here.

        except OphydAsyncNC as e:
            obj.connected = False  # ophyd-async does not provide the attribute 'connected' for its device. That is why added here.
            print(
                Fore.RED
                + f"Device: {obj.name}: Wait for connection ERRORED. Configured timeout value = {timeout}. Exception msg: {e}"
                + Style.RESET_ALL
            )

    def connect_to_sync_device(obj: object, timeout: float) -> None:
        """
        Connects a synchronous device within a specified timeout.

        This function waits for the given synchronous device to establish a connection
        within the provided timeout.

        Parameters
        ----------
        obj : object
            The synchronous device object that has a `wait_for_connection` method for establishing
            the connection.
        timeout : float
            The maximum time (in seconds) to wait for the connection.

        Returns
        -------
        None
            This function does not return a value but waits for the connection.

        Raises
        ------
        TimeoutError
            Raised if the device fails to connect within the specified timeout.

        Notes
        -----
        - This function is intended for devices that use the `wait_for_connection()` method to
        establish a connection.
        - If the connection attempt exceeds the provided timeout, a `TimeoutError` is raised.
        """

        try:
            obj.wait_for_connection(timeout=timeout)
        except TimeoutError as e:
            print(
                Fore.RED
                + f"Device: {obj.name}: Wait for connection ERRORED. Configured timeout value = {timeout}. Exception msg: {e}"
                + Style.RESET_ALL
            )

    print(Fore.BLUE + "Wait for device connection started." + Style.RESET_ALL)

    for obj in devices.values():
        timeout: float = obj.md.connection_timeout

        # OPHYD DEVICE
        if isinstance(obj, OphydDevice):
            connect_to_sync_device(obj, timeout=timeout)

        # OPHYD-ASYNC DEVICE
        elif isinstance(obj, OphydAsyncDevice):
            # Extract value of "mock_connection" if configured in dev. cfg. yml file
            try:
                mock: bool = obj.md["mock_connection"] == "True"
            except KeyError:
                mock = False  # Default case when 'connection_mock' not define in the device cfg yaml file

            connect_to_async_device(obj, timeout=timeout, mock=mock, run_engine = run_engine)

        # NOT SUPPORTED DEVICE
        else:
            print(
                Fore.RED
                + f"Connection to a device of type: {type(obj)} is not possible. Device is not supported. Supported device classes: {'ophyd', 'ophyd-async'}"
                + Style.RESET_ALL
            )

    # Print device state: connected/not connected
    for obj in devices.values():
        print(
            f"Device: {obj.name} of type: {type(obj)} is{' ' if obj.connected else Fore.RED + ' NOT '}connected{'.' if obj.connected else ' !!!' + Style.RESET_ALL}"
        )

    print(Fore.BLUE + "Wait for device connection finished." + Style.RESET_ALL)


def add_to_global_scope(objects: dict, global_scope: dict):
    """
    Add key-value pairs from a dictionary to the global scope as variables.

    This function takes a dictionary where the keys are intended to be the names
    of global variables and the values are the corresponding objects. It adds
    these key-value pairs to the global scope dictionary provided by `global_scope`.
    If a variable name already exists in the global scope, a ValueError is raised
    to prevent overwriting.

    Parameters
    ----------
    objects : dict
        A dictionary where each key is the intended name of a global variable and
        each value is the object to be assigned to that global variable name.

    global_scope : dict
        The dictionary representing the global scope where the variables should
        be added. Typically, this would be the dictionary returned by `globals()`.

    Raises
    ------
    ValueError
        If a key from the `objects` dictionary already exists as a key in the
        `global_scope` dictionary, a ValueError is raised to prevent overwriting
        existing global variables.

    Examples
    --------
        devices = {
            "device1": "instance1",
            "device2": "instance2",
        }
        add_to_global_scope(devices, globals())
        print(device1)  # Output: instance1
        print(device2)  # Output: instance2
    """

    for obj_name, obj_instance in objects.items():
        # Before adding a new object to the scope of global variables assert that it is does not already exist.
        # We do not want to overwrite already existing objects in the global scope

        if obj_name not in global_scope:
            global_scope[obj_name] = obj_instance
        else:
            raise ValueError(
                f"Add to the global scope an object with the name: {obj_name} FAILED. Object with the name: {obj_name} already exists in the scope of global variables."
            )


def _prepare_database(
    dev_cfg_list: list[dict], db_file_path: str, rml_file_path: str
) -> None:
    """create happi containers, create happi json database. Add containers into happi json database

    Parameters
    ----------
    dev_cfg_list : list[dict]
        Device configurations from cfg y(aml) file
    db_file_path : str
        Json database file path. E.g. ".../db.json"

    rml_file_path : str
        Path to rml file.

    Raises
    ------
    FileExistsError
        If "remove db file" FAILS
    ValueError
        If "create json db backend" FAILS
    ValueError
        If "get db client" FAILS
    """

    # Remove current database file before creating a new one with the same name
    if os.path.exists(db_file_path):
        os.remove(db_file_path)
        if os.path.exists(db_file_path):
            raise FileExistsError(
                f"File {db_file_path} could not be deleted by OS. Remedy: Delete it manualy."
            )

    # Create json database file
    create_json_db_backend(db_file_path)

    # Get db client
    client: Client = get_db_client(db_file_path)

    # Get metadata from rml file
    rml_metadata: dict[str, DeviceRmlMetaData] = get_md_from_rml(
        rml_file_path, dev_cfg_list
    )

    # Create custom happi containers
    containers: list = create_containers(dev_cfg_list, rml_metadata)

    # Add happi containers to database file
    ids = [client.add_item(container) for container in containers]
    [print(f"Happi container with id: {id} added to db.") for id in ids]


# ------------------------------------------------- End of FUNC DEFINITIONS -----------------------

if __name__ == "__main__":
    devices = instantiate()
    wait_for_device_connection(devices)

    # m1 = devices["m1"]
    # print(m1.md.worldPosition)
    # print(m1.md.desc)

    print(devices["m1"].connected is True)
    add_to_global_scope(devices, globals())
    print(m1.connected is True)
