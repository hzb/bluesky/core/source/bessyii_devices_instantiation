"""Module for management of device configuration y(a)ml file

Usage:          Import whole module or function(s) from the module. Supported functions:
                    - verify_dev_cfg()
                    - _is_value_unique()
"""

from colorama import Fore, Style

from bessyii_devices_instantiation.beamline_devices_const import (
    MANDATORY_KEY_NAMES,
    UNIQUE_VALUE_KEY_NAMES,
)


def verify_dev_cfg(dev_cfg_list: list[dict]):
    """Verify content of device configuration.
        - Check if dev_cfg_list is empty
        - Check if keys (defined in MANDATORY_KEY_NAMES) are present in every dict of the list "dev_cfg_list"
        - Check if values of all keys defined in UNIQUE_VALUE_KEY_NAMES are unique over all dicts of the list dev_cfg_list
            Skip the check if value of a special key defined in UNIQUE_VALUE_KEY_NAMES is an empty string or None

    Parameters
    ----------
    dev_cfg_list : list[dict]
        Device configurations from cfg y(aml) file

    Raises
    ------
    ValueError
        If dev_cfg_list is empty
    NameError
        If any mandatory key defined in MANDATORY_KEY_NAMES is missing in any of the configured devices

    Prints to stdout
    ------
        If value of keys defined in UNIQUE_VALUE_KEY_NAMES are not unique over all configured devices

    """

    if not dev_cfg_list:
        raise ValueError("Device cfg list cannot be empty. Check device cfg file.}")

    for key in MANDATORY_KEY_NAMES:
        key_found: list[bool] = [
            True if (key in item) else False for item in dev_cfg_list
        ]
        if not all(key_found):
            raise NameError(
                f"The mandatory key: '{key}' is missing in at least one of the devices configured in the device config yml file."
            )

    skip: list[str] = ["prefix"]
    for key in UNIQUE_VALUE_KEY_NAMES:
        if not _is_value_unique(key, dev_cfg_list, skip):
            print(
                Fore.RED
                + f"Some values of key: '{key}' in device config yml file are not unique. That could be intentional."
                + Style.RESET_ALL
            )


def _is_value_unique(key: str, dev_cfg_list: list[dict], skip: list[str]) -> bool:
    """Check if value corresponding to a given key is unique in the list of dictionaries

    Parameters
    ----------
    key : str
        Key name
    dev_cfg_list : list[dict]
        Device configurations from cfg y(aml) file
    skip : list
        List of keys for which check of uniqueness should be skipped if value of a key is empty or None
        Skip check of uniqueness if value of a key contained the list is empty

    Returns
    -------
    bool
        True/False key is/is not unique in the list[dict]
    """

    # Collection of values corresponding to the key
    values = [
        item[key]
        for item in dev_cfg_list
        if not ((key in skip) and ((item[key] == "") or (item[key] is None)))
    ]
    return len(values) == len(set(values))
